#!/usr/bin/env python

import zmq
import threading
import json

import time
import os
import sys

import pygame
from pygame.locals import *
from math import *

import argparse

import logging

logger = logging.getLogger()

PORT_COM = 6660

VERSION = "3.1"

LARGEUR_FENETRE = 600



class Config():
	_instance = None

	def __new__(classe, *args, **kargs): 
		if classe._instance is None:
			#classe._instance = object.__new__(classe, *args, **kargs)
			classe._instance = super(Config, classe).__new__(classe)
		return classe._instance

	def __init__(self):
		self.running = True
		self.nb_cases = 0
		self.largeur_fenetre = LARGEUR_FENETRE
		self.pause = False
		self.serie = False
		self.audio = False
		self.test = False
		self.couleur = 0
		self.bloque = False
		

conf = Config()

class ddoPygame(object):
	_instance = None

	def __new__(classe, *args, **kargs): 
		if classe._instance is None:
			#classe._instance = object.__new__(classe, *args, **kargs)
			classe._instance = super(ddoPygame, classe).__new__(classe)
		return classe._instance

	def __init__(self):
		context = zmq.Context()
		logger.debug("Context ZeroMQ créé")
		
		self.socket_emission = context.socket(zmq.REQ)
		self.socket_emission.connect ("tcp://" + args.HOTE + ":%s" % str(args.PORT_COM))
		self.socket_emission.setsockopt(zmq.RCVTIMEO, 500)
		logger.debug("Connection au serveur")
		
		self.socket_infos = context.socket(zmq.SUB)
		self.socket_infos.connect ("tcp://" + args.HOTE + ":%s" % str(args.PORT_COM+1))
		self.socket_infos.setsockopt_string(zmq.SUBSCRIBE, "")
		logger.debug("Inscription aux infos du serveur")
		
		self.socket_matrices = context.socket(zmq.SUB)
		self.socket_matrices.connect ("tcp://" + args.HOTE + ":%s" % str(args.PORT_COM+2))
		self.socket_matrices.setsockopt_string(zmq.SUBSCRIBE, "")
		logger.debug("Inscription aux matrices du serveur")

		logger.debug("Demande une connection au serveur")
		try:
			self.socket_emission.send_json({'__info__':'nb_cases'})
			conf.nb_cases = self.socket_emission.recv_json()
		except zmq.ZMQError as e:
			print("Impossible de joindre le serveur")
			sys.exit()
		self.socket_emission.send_json({'__info__':'pause'})
		conf.pause = self.socket_emission.recv_json()
		self.socket_emission.send_json({'__info__':'audio'})
		conf.audio = self.socket_emission.recv_json()
		self.socket_emission.send_json({'__info__':'test'})
		conf.test = self.socket_emission.recv_json()
		self.socket_emission.send_json({'__info__':'couleur'})
		conf.couleur = self.socket_emission.recv_json()
		self.socket_emission.send_json({'__info__':'serie'})
		conf.serie = self.socket_emission.recv_json()
		self.socket_emission.send_json({'__info__':'bloque'})
		conf.bloque = self.socket_emission.recv_json()
		self.largeur_case = int(conf.largeur_fenetre/conf.nb_cases)
		self._fichier = os.path.dirname(os.path.realpath(__file__))
		
		logger.debug("Lancement de l'interface")
		pygame.init()
		self.fenetre = pygame.display.set_mode((self.largeur_case*conf.nb_cases, self.largeur_case*conf.nb_cases))
		pygame.display.set_caption('Daftduino')
		pygame.mouse.set_visible(False)

		
		self.Infos = {}
		self.matrice = []
		# on rempli la matrice de noir, au cas ou le serveur soit en pause, ou s'il a un délai trop long
		for x in range(0, conf.nb_cases):
			Ligne = []
			for y in range(0, conf.nb_cases):
				Ligne.append((0,0,0))
			self.matrice.append(Ligne)
		self.verbose = False
		self.message = []
		self.aide = False
		self.affiche_menu = False
		self.dessine = False
		self.dessine_sym = 0

	def envoi_ordre(self, ordre, args):
		message = {'__ordre__':ordre, 'arguments': args}
		try:
			self.socket_emission.send_json(message)
			#  Get the reply.
			message = self.socket_emission.recv_json()
			if message != ordre:
				print(message)
		except:
			return False
		return

	def envoi_info(self, info, args):
		message = {'__info__':info, 'arguments': args}
		try:
			self.socket_emission.send_json(message)
			#  Get the reply.
			message = self.socket_emission.recv_json()
		except:
			return False 
		return message

	def run(self, verbose):
		self.verbose = verbose
		Thread_Reception = threading.Thread(target=self.Reception_Infos, daemon=True)
		Thread_Reception.start()
		logger.debug("Thread de réception lancé")
		
		Thread_ReceptionM = threading.Thread(target=self.Reception_Matrices, daemon=True)
		Thread_ReceptionM.start()
		logger.debug("Thread de réceptionM lancé")
		
		thread_evenements = threading.Thread(target=self.gere_evenements, daemon=True)
		thread_evenements.start()
		
		thread_affichage = threading.Thread(target=self.gere_affichage, daemon=True)
		thread_affichage.start()
		
		while conf.running:
			time.sleep(0.5)

	def gere_affichage(self):
		while conf.running:
			self.affiche(self.matrice)
			time.sleep(0.1)
		
		
	def affiche(self, Matrice):
		TexteInfos = pygame.font.Font(pygame.font.match_font('liberationmono'), 14)
		TexteTitre = pygame.font.Font(pygame.font.match_font('liberationmono'), 30)
		for x in range(0, conf.nb_cases):
			for y in range(0, conf.nb_cases):
				Xd = x * self.largeur_case
				Yd = y * self.largeur_case
				couleur = Matrice[x][y]
				CouleurBis = (int(couleur[0]/2),int(couleur[1]/2),int(couleur[2]/2))
				pygame.draw.rect(self.fenetre, CouleurBis, pygame.Rect(Xd, Yd, self.largeur_case, self.largeur_case))
				Rayon = int(max(couleur[0],couleur[1],couleur[2])*(self.largeur_case/2)/255)
				pygame.draw.circle(self.fenetre, couleur, (int(Xd+self.largeur_case/2), int(Yd+self.largeur_case/2)), Rayon)
		if conf.pause:
			icone = pygame.image.load(self._fichier + "/pause.png").convert_alpha()
			self.fenetre.blit(icone,(self.largeur_case*conf.nb_cases/2-130,self.largeur_case*conf.nb_cases/2-130))
		if conf.couleur == 0:
			icone = pygame.image.load(self._fichier + "/icone_c0.png").convert_alpha()
		elif conf.couleur == 1:
			icone = pygame.image.load(self._fichier + "/icone_c1.png").convert_alpha()
		else:
			icone = pygame.image.load(self._fichier + "/icone_c2.png").convert_alpha()
		self.fenetre.blit(icone,(self.largeur_case*conf.nb_cases-40,self.largeur_case*conf.nb_cases-40))
		nombre_icones = 1
		if conf.serie:
			icone = pygame.image.load(self._fichier + "/icone_serie.png").convert_alpha()
			self.fenetre.blit(icone,(self.largeur_case*conf.nb_cases-(40+40*nombre_icones),self.largeur_case*conf.nb_cases-40))
			nombre_icones += 1
		if conf.audio:
			icone = pygame.image.load(self._fichier + "/icone_audio.png").convert_alpha()
			self.fenetre.blit(icone,(self.largeur_case*conf.nb_cases-(40+40*nombre_icones),self.largeur_case*conf.nb_cases-40))
			nombre_icones += 1
		if conf.test:
			icone = pygame.image.load(self._fichier + "/icone_test.png").convert_alpha()
			self.fenetre.blit(icone,(self.largeur_case*conf.nb_cases-(40+40*nombre_icones),self.largeur_case*conf.nb_cases-40))
			nombre_icones += 1
		if conf.bloque:
			icone = pygame.image.load(self._fichier + "/icone_bloque.png").convert_alpha()
			self.fenetre.blit(icone,(self.largeur_case*conf.nb_cases-(40+40*nombre_icones),self.largeur_case*conf.nb_cases-40))
			nombre_icones += 1
		

		if self.aide:
			s = pygame.Surface((self.largeur_case*conf.nb_cases, self.largeur_case*conf.nb_cases), pygame.SRCALPHA) 
			s.fill((0,0,0,120))
			self.fenetre.blit(s,(0,0))
			Aide = ["p : Pause",
				"Entrer/Espace : Zapper",
				"↑ : Augmenter le delta de teinte",
				"↓: Réduire le delta de teinte",
				"→: Décaler la teinte sens horaire",
				"←: Décaler la teinte sens anti-horaire",
				"+: Augmenter le délai",
				"-: Réduire le délai",
				"v: Mode verbeux",
				"h: Aide",
				"m: Menu d'animations",
				"b: Rester sur cette animation",
				"*: Augmenter le pas de décalage de la teinte",
				"/: Réduire le pas de décalage de la teinte",
				"c: Modifier le filtre de couleur",
				"d: Mode dessin",
				"s: change la symétrie du mode Dessin",
				"CTRL+s: Activer/désactiver la sortie série",
				"CTRL+a: Activer/désactiver les animations audio",
				"CTRL+t: Activer/désactiver les animations en test",
				"ESC: Fermer la fenêtre",
				"CTRL+ESC: Arêter le serveur"
			]
			Hauteur = 100
			for MessageMenu in Aide:
				Textel = TexteInfos.render(MessageMenu,True,(255,255,255))
				TexteRect = Textel.get_rect()
				TexteRect.centerx = self.fenetre.get_rect().centerx
				TexteRect.centery = Hauteur
				self.fenetre.blit(Textel,TexteRect)
				Hauteur += 20
			TitreAide = TexteTitre.render("Aide",True,(255,255,255))
			TexteRect = TitreAide.get_rect()
			TexteRect.centerx = self.fenetre.get_rect().centerx
			TexteRect.centery = 60
			self.fenetre.blit(TitreAide,TexteRect)
		elif self.affiche_menu:
			s = pygame.Surface((self.largeur_case*conf.nb_cases, self.largeur_case*conf.nb_cases), pygame.SRCALPHA) 
			s.fill((0,0,0,120))
			self.fenetre.blit(s,(0,0))
			Menu = self.envoi_info('menu', None)
			if Menu:
				Menu = Menu.split('\n')
				Titre = Menu[0]
				Menu.pop(0)
				Hauteur = 100
				for MessageMenu in Menu:
					#MessageMenu#TODO: virer le tab, et ce qu'il y a avant
					if MessageMenu[0:2] == "->":
						MessageMenu = "# " + MessageMenu[3:] + " #"
					Textel = TexteInfos.render(MessageMenu.strip(),True,(255,255,255))
					TexteRect = Textel.get_rect()
					TexteRect.centerx = self.fenetre.get_rect().centerx
					TexteRect.centery = Hauteur
					self.fenetre.blit(Textel,TexteRect)
					Hauteur += 15
				TitreAide = TexteTitre.render(Titre,True,(255,255,255))
				TexteRect = TitreAide.get_rect()
				TexteRect.centerx = self.fenetre.get_rect().centerx
				TexteRect.centery = 60
				self.fenetre.blit(TitreAide,TexteRect)
		elif self.dessine:
			coordonnees = []
			(posX, posY) = pygame.mouse.get_pos()
			
			largeur = self.largeur_case*conf.nb_cases
			
			coordonnees.append((posX/largeur, posY/largeur))
			s = pygame.Surface((largeur, largeur))
				
			if self.dessine_sym == 1 or self.dessine_sym == 4:
				pygame.draw.rect(s, (255,255,255), pygame.Rect(largeur/2-2, 0, 4, largeur))
				pygame.draw.rect(s, (0,0,0), pygame.Rect(largeur/2-1, 0, 2, largeur))
			
			if self.dessine_sym == 2 or self.dessine_sym == 4:
				pygame.draw.rect(s, (255,255,255), pygame.Rect(0, largeur/2-2, largeur, 4))
				pygame.draw.rect(s, (0,0,0), pygame.Rect(0, largeur/2-1, largeur, 2))
				
			if self.dessine_sym == 3 or self.dessine_sym == 4:
				pygame.draw.rect(s, (255,255,255), pygame.Rect(largeur/4-2, largeur/2-2, largeur/2, 4))
				pygame.draw.rect(s, (255,255,255), pygame.Rect(largeur/2-2, largeur/4-2, 4, largeur/2))
				pygame.draw.rect(s, (0,0,0), pygame.Rect(largeur/4-2, largeur/2-1, largeur/2, 2))
				pygame.draw.rect(s, (0,0,0), pygame.Rect(largeur/2-1, largeur/4-2, 2, largeur/2))
			s.set_alpha(50)
			self.fenetre.blit(s,(0,0))
			
			if self.dessine_sym == 1 or self.dessine_sym == 4:
				pygame.draw.circle(self.fenetre, (0,0,0), (largeur - posX, posY), 10, 0)
				pygame.draw.circle(self.fenetre, (255,255,0), (largeur - posX, posY), 7, 0)
				coordonnees.append((1 - posX/largeur, posY/largeur))
			
			if self.dessine_sym == 2 or self.dessine_sym == 4:
				pygame.draw.circle(self.fenetre, (0,0,0), (posX, largeur - posY), 10, 0)
				pygame.draw.circle(self.fenetre, (255,255,0), (posX, largeur - posY), 7, 0)
				coordonnees.append((posX/largeur, 1 - posY/largeur))
				
			if self.dessine_sym == 3 or self.dessine_sym == 4:
				pygame.draw.circle(self.fenetre, (0,0,0), (largeur - posX, largeur - posY), 10, 0)
				pygame.draw.circle(self.fenetre, (255,255,0), (largeur - posX, largeur - posY), 7, 0)
				coordonnees.append((1 - posX/largeur, 1 - posY/largeur))
				

			pygame.draw.circle(self.fenetre, (0,0,0), (posX, posY) , 10, 0)
			pygame.draw.circle(self.fenetre, (255,0,0), (posX, posY) , 7, 0)
			
			self.envoi_ordre('position', coordonnees)

		if self.verbose:
			Infos = self.Infos['__infos_matrice__']
			if Infos == {}:
				logger.warning("On n'a pas d'infos...")
			else:
				s = pygame.Surface((self.largeur_case*conf.nb_cases, 40), pygame.SRCALPHA) 
				s.fill((0,0,0,120))
				self.fenetre.blit(s,(0,0))
				cercle = pygame.image.load(self._fichier + "/cercle.png").convert_alpha()
				self.fenetre.blit(cercle,(self.largeur_case*conf.nb_cases-120,0))
				# je fais 3 fois le même arc de cercle pour éviter que ça fasse des trucs dégueulasses
				pygame.draw.arc(self.fenetre, (255,255,255),[self.largeur_case*conf.nb_cases+16-120, 16, 88, 88], \
					2*pi*Infos['TeinteP']-Infos['DeltaTeinteP']*2*pi, \
					2*pi*Infos['TeinteP']+Infos['DeltaTeinteP']*2*pi,3)
				pygame.draw.arc(self.fenetre, (255,255,255),[self.largeur_case*conf.nb_cases+16-120, 16, 88, 88], \
					2*pi*Infos['TeinteP']-Infos['DeltaTeinteP']*2*pi+0.01, \
					2*pi*Infos['TeinteP']+Infos['DeltaTeinteP']*2*pi+0.01,3)
				pygame.draw.arc(self.fenetre, (255,255,255),[self.largeur_case*conf.nb_cases+16-120, 16, 88, 88], \
					2*pi*Infos['TeinteP']-Infos['DeltaTeinteP']*2*pi-0.01, \
					2*pi*Infos['TeinteP']+Infos['DeltaTeinteP']*2*pi-0.01,3)
				Texte1 = TexteInfos.render("Teinte:" + str(float(Infos['TeinteP'].__round__(4))) + \
					" | DeltaTeinte:" + str(float(Infos['DeltaTeinteP'].__round__(4))) + " | Variation:" \
					+ str(float(Infos['VariationTeinteP'].__round__(1))) + " | Delai:" + str(Infos['Delai']) ,
					True,(255,255,255))
				Texte2 = TexteInfos.render(Infos['Animation'] + " (" + str(Infos['Frame']) + "/" + str(Infos['FramesTotal']) + ")",True,(255,255,255))
				self.fenetre.blit(Texte1,(0,0))
				self.fenetre.blit(Texte2,(0,20))
				point = pygame.image.load(self._fichier + "/point.png").convert_alpha()
				Centre_X = self.largeur_case*conf.nb_cases-60
				Centre_Y = 60
				Pos_X = 42*cos(2*pi*Infos['TeinteP'])
				Pos_Y = 42*sin(2*pi*Infos['TeinteP'])
				self.fenetre.blit(point,(Centre_X+Pos_X-6,Centre_Y-Pos_Y-6))
			for i in range(0, len(self.message)):
				Texte_notifs = TexteInfos.render(str(self.message[i]), True, (200-(len(self.message)-i)*20,200-(len(self.message)-i)*20,200-(len(self.message)-i)*20))
				self.fenetre.blit(Texte_notifs,(0,self.largeur_case*conf.nb_cases-(len(self.message)-i)*15))
		pygame.display.flip()

	def gere_evenements(self):
		while conf.running:
			for event in pygame.event.get():
				if event.type == pygame.QUIT: 
					conf.running = False
				elif event.type == KEYUP:
					if self.affiche_menu:
						if event.key == K_ESCAPE and self.affiche_menu: 
							self.affiche_menu = False
						elif event.key == K_m:
							self.affiche_menu = not self.affiche_menu
						elif event.key == K_RIGHT or event.key == K_KP_ENTER or event.key == K_RETURN:#select True (zap)
							self.envoi_ordre('menu_select', True)
						elif event.key == K_LEFT or event.key == K_BACKSPACE:
							self.envoi_ordre('menu_retour', True)
						elif event.key == K_UP:
							self.envoi_ordre('menu_precedent', True)
						elif event.key == K_DOWN:
							self.envoi_ordre('menu_suivant', True)
						elif event.key == K_a:
							self.envoi_ordre('menu_select', False)
					else:
						if event.key == K_p: 
							self.envoi_ordre('pause', not self.envoi_info('pause', None))
						elif event.key == K_v:
							self.verbose = not self.verbose
						elif event.key == pygame.K_ESCAPE and pygame.key.get_mods() & pygame.KMOD_CTRL:
							self.envoi_ordre('stop', None)
							conf.running = False
						elif event.key == K_s and pygame.key.get_mods() & pygame.KMOD_CTRL: 
							self.envoi_ordre('serie', not self.envoi_info('serie', None))
						elif event.key == K_t and pygame.key.get_mods() & pygame.KMOD_CTRL: 
							self.envoi_ordre('test', not self.envoi_info('test', None))
						elif event.key == K_a and pygame.key.get_mods() & pygame.KMOD_CTRL: 
							self.envoi_ordre('audio', not self.envoi_info('audio', None))
						elif event.key == K_c: 
							self.envoi_ordre('couleur', self.envoi_info('couleur', None)+1)
						elif event.key == K_ESCAPE and not self.aide and not self.affiche_menu: 
							conf.running = False
						elif event.key == K_ESCAPE and self.aide: 
							self.aide = False
						elif event.key == K_ESCAPE and self.affiche_menu: 
							self.affiche_menu = False
						elif event.key == K_KP_PLUS:
							delai = self.envoi_info('delai', None) + 10
							self.envoi_ordre('delai', str(delai))
						elif event.key == K_KP_MINUS:
							delai = self.envoi_info('delai', None) - 10
							if delai < 0: delai = 0
							self.envoi_ordre('delai', str(delai))
						elif event.key == K_LEFT:
							teinte = (self.envoi_info('teinte', None) + 0.01)%1
							self.envoi_ordre('teinte', str(teinte))
						elif event.key == K_RIGHT:
							teinte = (self.envoi_info('teinte', None) - 0.01)%1
							self.envoi_ordre('teinte', str(teinte))
						elif event.key == K_UP:
							delta_teinte = self.envoi_info('delta_teinte', None) + 0.01
							if delta_teinte > 0.5 : delta_teinte = 0.5
							self.envoi_ordre('delta_teinte', str(delta_teinte))
						elif event.key == K_DOWN:
							delta_teinte = self.envoi_info('delta_teinte', None) - 0.01
							if delta_teinte < 0 : delta_teinte = 0
							self.envoi_ordre('delta_teinte', str(delta_teinte))
						elif event.key == K_KP_MULTIPLY: # *
							variation_teinte = self.envoi_info('variation_teinte', None) + 1
							self.envoi_ordre('variation_teinte', str(variation_teinte))
						elif event.key == K_KP_DIVIDE:# /
							variation_teinte = self.envoi_info('variation_teinte', None) - 1
							self.envoi_ordre('variation_teinte', str(variation_teinte))
						elif event.key == K_SPACE or event.key == K_KP_ENTER or event.key == K_RETURN:
							self.envoi_ordre('zap',None)
						elif event.key == K_h:
							self.aide = not self.aide
						elif event.key == K_b: 
							self.envoi_ordre('bloque', not self.envoi_info('bloque', None))
						elif event.key == K_m and not self.aide:
							self.affiche_menu = not self.affiche_menu
						elif event.key == K_d:
							self.dessine = not self.dessine
						elif event.key == K_s:
							  self.dessine_sym = (self.dessine_sym + 1)%5

	def Reception_Infos(self):
		while conf.running:
			message = self.socket_infos.recv_json()
			if "__info__" in message:
				if "stop" in message["__info__"]:# and message["__info__"]["stop"] == True:
					logger.info("Signal de fin reçu du serveur")
					conf.running = False
				elif "pause" in message["__info__"]:
					conf.pause = message["__info__"]["pause"]
				elif "audio" in message["__info__"]:
					conf.audio = message["__info__"]["audio"]
				elif "test" in message["__info__"]:
					conf.test = message["__info__"]["test"]
				elif "couleur" in message["__info__"]:
					conf.couleur = message["__info__"]["couleur"]
				elif "serie" in message["__info__"]:
					conf.serie = message["__info__"]["serie"]
				elif "bloque" in message["__info__"]:
					conf.bloque = message["__info__"]["bloque"]
				elif "zap" in message["__info__"]:
					pass
				elif "ajout" in message["__info__"]:
					pass
				else:
					self.ajout_message(message)
			elif '__infos_matrice__' in message:
				self.Infos = message
			else:
				self.ajout_message(message)

	def ajout_message(self, message):
		self.message.append(message)
		if len(self.message) == 6:
			self.message.pop(0)
		
	
	def Reception_Matrices(self):
		while conf.running:
			matrice = self.socket_matrices.recv_json()
			self.matrice = matrice
			#self.affiche(matrice)

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Client en ligne de commande pour DaftDuino', add_help=False)
	options = parser.add_argument_group("Options")
	options.add_argument('-h', '--help', '--aide', action='help', help="Affiche cette aide, et s'en va")
	options.add_argument('-p', '--port', action='store', type=int, help="Port à utiliser pour la communication avec le serveur", default=PORT_COM, dest='PORT_COM', metavar='XXX')
	options.add_argument('-hote', action='store', help="Hote du serveur", default="localhost", dest='HOTE')
	options.add_argument('-V', action='store_true', help="Affiche les infos dans la fenêtre", default=False)
	options.add_argument('-v', '--verbose', action='count', help="Mode bavard")
	options.add_argument('--version', action='version', help="Affiche la version du programme", version='%(prog)s '+ VERSION)
	
	args = parser.parse_args()
	NIVEAU_LOG = logging.ERROR
	if args.verbose == 1:
		NIVEAU_LOG = logging.WARNING
	elif args.verbose == 2:
		NIVEAU_LOG = logging.INFO
	elif args.verbose == 3:
		NIVEAU_LOG = logging.DEBUG
	
	logger.setLevel(NIVEAU_LOG)
	# format avec lequel les logs vont s'afficher
	formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
	# pour rediriger les logs vers la console :
	steam_handler = logging.StreamHandler()
	steam_handler.setLevel(logging.DEBUG)
	steam_handler.setFormatter(formatter)
	logger.addHandler(steam_handler)
		
	pyddo = ddoPygame()
	pyddo.run(args.V)
