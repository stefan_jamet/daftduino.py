import random
import colorsys
import math

R = 0
G = 1
B = 2

class Couleur(list):
	"""
	Classe pour gérer les couleurs.
	Les attributs de classe gèrent la couleur générale demandée par le programme

	Initialisation :
		Couleur() : donne une couleur au hasard (dans les limites générales)
		Couleur(r,v,b) ou Couleur(teinte): donne la couleur spécifiée

	Cercle chromatique :
		Rouge:		0°		0.0
		Orange:		30°		0.125
		Jaune:		60°		0.25
		Vert:		120°	0.375
		Cyan:		180°	0.5
		Bleu:		240°	0.625
		Violet:		270°	0.75
		Magenta:	300°	0.875
		Rouge:		360°	1.0
	"""
	teinte_generale = random.random() # met la teinte générale quelque part sur le cercle chromatique
	delta_teinte_generale = 0.5 # autorise tout le cercle (+-0.5 = 1)
	variation_teinte_generale = random.randint(-30,30) # on decale la teinte de pas grand chose a chaque fois
	
	def __init__(self, *args):
		if len(args) == 3:
			self._r = self.rgb_min_max(args[R])
			self._g = self.rgb_min_max(args[G])
			self._b = self.rgb_min_max(args[B])
			self._rgb_to_hsv()
		elif len(args) == 1:
			self._h = self.teinte_min_max(args[0])
			self._s, self._v = 0, 0
			self._hsv_to_rgb()
		else:
			self.alea()
		# Pour réagir comme un tableau :
		self.append(self._r)
		self.append(self._g)
		self.append(self._b)

	def alea(self):
		self._h = self.teinte_min_max(random.uniform(Couleur.teinte_generale - Couleur.delta_teinte_generale, \
			Couleur.teinte_generale + Couleur.delta_teinte_generale))
		self._s = self.hsv_min_max(random.uniform(7/8+Couleur.delta_teinte_generale/4, 1))
		self._v = self.hsv_min_max(random.uniform(7/8+Couleur.delta_teinte_generale/4, 1))
		self._hsv_to_rgb()
		return self

	def _rgb_to_hsv(self):
		' Convertit une couleur RGB (int de 0 à 255) en HSV (float de 0 à 1)'
		self._h, self._s, self._v = colorsys.rgb_to_hsv(self._r/255, self._g/255, self._b/255)

	def _hsv_to_rgb(self):
		' Convertit une couleur HSV (float de 0 à 1) en RGB (int de 0 à 255)'
		r, g, b = colorsys.hsv_to_rgb(self._h, self._s, self._v)
		self._r = int(r*255)
		self._g = int(g*255)
		self._b = int(b*255)
	
	@classmethod
	def variation(cls):
		cls.teinte_generale = cls.teinte_min_max(cls.teinte_generale \
			+ cls.variation_teinte_generale/10000)
	
	def varie(self, Cible = None):# Pas sur que ça marche ça...
		' Fait varier la couleur suivant la variation_teinte_generale, ou tend vers la couelur spécifiée'
		Pas = 1/100
		if type(Cible) == type(Couleur()):
			if self._h == Cible._h and self._s == Cible._s and self._v == Cible._v:
				# Si la couleur est atteinte, on ne fait rien
				pass
			else:
				if self._s == 0 or Cible._s == 0:
				# on va ou on vient d'un gris, du coups le gris prend la teinte de l'autre
					if self._h < Cible._h:
						self._h = Cible._h
					else:
						Cible._h = self._h
				if self._h != Cible._h:
					if (self._h + Pas) > Cible._h and (self._h - Pas) < Cible._h: self._h = Cible._h
					elif abs(self._h - Cible._h) <= 0.5:
						if self._h < Cible._h:
							self._h += Pas
						else:
							self._h -= Pas
					else:
						if self._h < Cible._h:
							self._h -= Pas
						else:
							self._h += Pas
					self._h = self.teinte_min_max(self._h)
				if self._s != Cible._s:
					if (self._s + Pas) > Cible._s and (self._s - Pas) < Cible._s:
						self._s = Cible._s
					elif self._s > Cible._s:
						self._s -= Pas
					else:
						self._s += Pas
					self._s = self.hsv_min_max(self._s)
				if self._v != Cible._v:
					if (self._v + Pas) > Cible._v and (self._v - Pas) < Cible._v:
						self._v = Cible._v
					elif self._v > Cible._v:
						self._v -= Pas
					else:
						self._v +=Pas
					self._v = self.hsv_min_max(self._v)
		else:# on évolue de la même manière que la teinte générale
			self._h = self.teinte_min_max(self._h + Couleur.variation_teinte_generale/10000)
			#self.alea()
		self._hsv_to_rgb()
		return self

	@staticmethod
	def rgb_min_max(x):
		' Contraint une valeur RGB a être entre 0 et 255 '
		return max(min(255, int(x)), 0)

	@staticmethod
	def teinte_min_max(x):
		' Contraint la teinte (Hue) à être entre 0 et 1, sans être 0 ou 1 si la valeur de base déborde '
		x = x - math.floor(x)
		if x < 0:
			x += 1
		elif x > 1:
			x -= 1
		return float(x)

	@staticmethod
	def hsv_min_max(x):
		' Contraint une valeur HSV a être entre 0 et 1 '
		return max(min(1, float(x)), 0)

	# Imitation du fonctionnement d'un tableau
	def __getitem__(self, key):
		"""
			Est appelé quand on fait objet[index] ou objet[key].
			Utile pour simuler une liste ou un dico.
		"""
		if key == R:
			return self._r
		elif key == G:
			return self._g
		elif key == B:
			return self._b
		else: return None

	def __setitem__(self, key, value):
		"""
			Est appelé quand on fait objet[index]=value ou objet[key]=value.
			Utile pour simuler une liste ou un dico.
		"""
		if key == R:
			self._r = self.rgb_min_max(value)
		elif key == G:
			self._g = self.rgb_min_max(value)
		elif key == B:
			self._b = self.rgb_min_max(value)
		self._rgb_to_hsv()

#	# Surcharge des opérateurs. Vérifier l'utilité de ça...
	def __add__(self,CouleurArg):
		""" Surcharge de l'operateur + """
		if type(CouleurArg) == type(Couleur()):
			Rouge = self.rgb_min_max(self._r + CouleurArg._r)
			Vert = self.rgb_min_max(self._g + CouleurArg._g)
			Bleu = self.rgb_min_max(self._b + CouleurArg._b)
		else:
			Rouge = self.rgb_min_max(self._r + CouleurArg)
			Vert = self.rgb_min_max(self._g + CouleurArg)
			Bleu = self.rgb_min_max(self._b + CouleurArg)
		return Couleur(Rouge,Vert,Bleu)

	def __sub__(self,CouleurArg):
		""" Surcharge de l'operateur - """
		if type(CouleurArg) == type(Couleur()):
			Rouge = self.rgb_min_max(self._r - CouleurArg._r)
			Vert = self.rgb_min_max(self._g - CouleurArg._g)
			Bleu = self.rgb_min_max(self._b - CouleurArg._b)
		else:
			Rouge = self.rgb_min_max(self._r - CouleurArg)
			Vert = self.rgb_min_max(self._g - CouleurArg)
			Bleu = self.rgb_min_max(self._b - CouleurArg)
		return Couleur(Rouge,Vert,Bleu)
	
	def __mul__(self,CouleurArg): # *
		Max = max(self._r, CouleurArg._r, self._g, CouleurArg._g, self._b, CouleurArg._b)
		Rouge = self.rgb_min_max(self._r * CouleurArg._r/Max)
		Vert = self.rgb_min_max(self._g * CouleurArg._g/Max)
		Bleu = self.rgb_min_max(self._b * CouleurArg._b/Max)
		return Couleur(Rouge,Vert,Bleu)
#	def __mul__(self,CouleurArg):# utile ???
#		""" Surcharge de l'operateur * """
#		if type(CouleurArg) == type(Couleur()):
#			Max = max(self._r, CouleurArg._r, self._g, CouleurArg._g, self._b, CouleurArg._b)
#			Rouge = self.rgb_min_max(self._r * CouleurArg._r/Max)
#			Vert = self.rgb_min_max(self._g * CouleurArg._g/Max)
#			Bleu = self.rgb_min_max(self._b * CouleurArg._b/Max)
#		else:
#			Rouge = self.rgb_min_max(self._r * CouleurArg)
#			Vert = self.rgb_min_max(self._g * CouleurArg)
#			Bleu = self.rgb_min_max(self._b * CouleurArg)
#		return Couleur(Rouge,Vert,Bleu)

#	def __div__(self,CouleurArg):# utile ???
#		""" Surcharge de l'operateur / """
#		if type(CouleurArg) == type(Couleur()):
#			Rouge = self.rgb_min_max(self._r / CouleurArg._r)
#			Vert = self.rgb_min_max(self._g / CouleurArg._g)
#			Bleu = self.rgb_min_max(self._b / CouleurArg._b)
#		else:
#			Rouge = self.rgb_min_max(self._r / CouleurArg)
#			Vert = self.rgb_min_max(self._g / CouleurArg)
#			Bleu = self.rgb_min_max(self._b / CouleurArg)
#		return Couleur(Rouge,Vert,Bleu)