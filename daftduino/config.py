TOUT = 0
MIX = 1
RGB = 2

class Config(object):
	_instance = None

	def __new__(classe, *args, **kargs): 
		if classe._instance is None:
			classe._instance = object.__new__(classe, *args, **kargs)
		return classe._instance

	def __init__(self):
		self.nb_cases = 0
		self.delai = 0
		self.fin = False
		self.pause = False
		self.zap = False
		self.serie = False
		self._serie_prete = False
		
		self.socket_infos = None
		self.socket_matrices = None
		
		self.sortie_serie = None
		
		self.bloque = False
		
		# Filtres animations
		self._couleur = TOUT
		self._audio = False
		self._test = False
		# TODO Penser à faire un truc pour mettre le menu et la liste des animations à jour quand ces trucs sont modifiés
		
		self._positions = []

config_generale = Config()