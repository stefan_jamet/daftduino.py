import time
import copy
import random
from math import *

from .base import Anims
from couleur import Couleur

TOUT = 0
MIX = 1
RGB = 2

class Snakes(Anims):
	def play_std(self, MatriceBase, kwargs={}):
		kwargs['Frames'] = kwargs.get('Frames', random.randint(100,200))
		kwargs['Estompage'] = kwargs.get('Estompage', 7)
		kwargs['Quantite'] = kwargs.get('Quantite', 1)
		kwargs['Sens'] = kwargs.get('Sens', 0) # 0: sens = tous les sens, 1: 1 seul sens pour tous les snakes
		
		if kwargs['Quantite'] < 1: kwargs['Quantite'] = 1
		CouleurCible = []
		x = []
		y = []
		Direction = []
		Direction2 = []
		for idx in range(0,kwargs['Quantite']):
			CouleurCible.append(Couleur())
			x.append(random.randrange(0,self.conf.nb_cases))
			y.append(random.randrange(0,self.conf.nb_cases))
			Direction.append(random.randint(0,3))
			Direction2.append(random.randint(0,3))
			if idx > 0 and kwargs['Sens'] == 1:
				Direction[idx] = Direction[0]
				Direction2[idx] = Direction2[0]
		for Frames in range(0,kwargs['Frames']):
			start = time.perf_counter()
			dic_infos = Anims.infos(self, kwargs['Frames'], Frames, kwargs)
			MatriceBase.estompe(kwargs['Estompage'])
			for idx in range(0,kwargs['Quantite']):
				Direction2[idx] = random.randint(0,3)
				if idx > 0 and kwargs['Sens'] == 1:
					Direction2[idx] = Direction2[0]
				if Direction2[idx] != (Direction[idx] - 2) % 4:
					# Pour empecher le snake de faire demi-tour
					Direction[idx] = Direction2[idx]
				if Direction[idx] == 0:
					y[idx] -= 1
				elif Direction[idx] == 1:
					x[idx] += 1
				elif Direction[idx] == 2:
					y[idx] += 1
				else:
					x[idx] -= 1
				MatriceBase[x[idx]%self.conf.nb_cases][y[idx]%self.conf.nb_cases] = MatriceBase[x[idx]%self.conf.nb_cases][y[idx]%self.conf.nb_cases] + CouleurCible[idx]
				CouleurCible[idx].varie()
			self.affiche(MatriceBase, dic_infos, self.conf.delai, start)
			if self.conf.fin or self.conf.zap:
				return MatriceBase
			if self.conf.pause:
				while self.conf.pause:
					if self.conf.fin == True:
						break
					time.sleep(0.1)
		return MatriceBase
	def play_rgb(self, MatriceBase, kwargs={}):
		kwargs['Frames'] = kwargs.get('Frames', random.randint(100,200))
		kwargs['Estompage'] = kwargs.get('Estompage', 10)
		kwargs['Sens'] = kwargs.get('Sens', 0) # 0: sens = tous les sens, 1: 1 seul sens pour tous les snakes
		
		CouleurCible = Couleur()
		x = []
		y = []
		Direction = []
		Direction2 = []
		for color in range(0,3):
			x.append(random.randrange(0,self.conf.nb_cases))
			y.append(random.randrange(0,self.conf.nb_cases))
			Direction.append(random.randint(0,3))
			Direction2.append(random.randint(0,3))
			if color > 0 and kwargs['Sens'] == 1:
				Direction[color] = Direction[0]
				Direction2[color] = Direction2[0]
		for Frames in range(0,kwargs['Frames']):
			start = time.perf_counter()
			dic_infos = Anims.infos(self, kwargs['Frames'], Frames, kwargs)
			MatriceBase.estompe(kwargs['Estompage'])
			for color in range(0,3):
				Direction2[color] = random.randint(0,3)
				if color > 0 and kwargs['Sens'] == 1:
					Direction2[color] = Direction2[0]
				if Direction2[color] != (Direction[color] - 2) % 4:
					# Pour empecher le snake de faire demi-tour
					Direction[color] = Direction2[color]
				if Direction[color] == 0:
					y[color] -= 1
				elif Direction[color] == 1:
					x[color] += 1
				elif Direction[color] == 2:
					y[color] += 1
				else:
					x[color] -= 1
				MatriceBase[x[color]%self.conf.nb_cases][y[color]%self.conf.nb_cases][color] = MatriceBase[x[color]%self.conf.nb_cases][y[color]%self.conf.nb_cases][color] + CouleurCible[color]
			CouleurCible.varie()
			self.affiche(MatriceBase, dic_infos, self.conf.delai, start)
			if self.conf.fin or self.conf.zap:
				return MatriceBase
			if self.conf.pause:
				while self.conf.pause:
					if self.conf.fin == True:
						break
					time.sleep(0.1)
		return MatriceBase

class Snake(Snakes):
	_couleur = MIX
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		obj = Snakes()
		MatriceBase = obj.play_std(MatriceBase, kwargs)
		return MatriceBase
class Snake_RGB(Snakes):
	_couleur = RGB
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		obj = Snakes()
		MatriceBase = obj.play_rgb(MatriceBase, kwargs)
		return MatriceBase
class Snake_RGB_OneD(Snakes):# ça marche ça ?????????
	_couleur = RGB
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		kwargs['Sens'] = 1
		obj = Snakes()
		MatriceBase = obj.play_rgb(MatriceBase, kwargs)
		return MatriceBase
class Snakes_Multi(Snakes):
	_couleur = MIX
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		kwargs['Quantite'] = kwargs.get('Quantite', random.randint(2,4))
		obj = Snakes()
		MatriceBase = obj.play_std(MatriceBase, kwargs)
		return MatriceBase
class Snakes_Multi_OneD(Snakes):
	_couleur = MIX
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		kwargs['Quantite'] = kwargs.get('Quantite', random.randint(2,4))
		kwargs['Sens'] = 1
		obj = Snakes()
		MatriceBase = obj.play_std(MatriceBase, kwargs)
		return MatriceBase