import time
import serial

from couleur import Couleur

from config import config_generale
from logger import logger

TOUT = 0
MIX = 1
RGB = 2

class Anims(object):
	coeff = 10
	_audio = False
	_test = False
	_couleur = TOUT # Mix / RGB / Tout
	
	def __init__(self):
		self.conf = config_generale
		
	def play(self, MatriceBase, kwargs={}):
		logger.warning('Prototype de la fonction Play. Vous ne devriez pas voir ça.')
		return MatriceBase
	
	def infos(self, frames_tot, frame, kwargs={}):
		if self.conf.fin:
			return 'Fin'
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		return {'__infos_matrice__':{'Frame':frame,
			'FramesTotal':frames_tot,
			'Animation': kwargs['_nom'],
			'Delai':self.conf.delai,
			'TeinteP': Couleur.teinte_generale,
			'DeltaTeinteP':Couleur.delta_teinte_generale,
			'VariationTeinteP':Couleur.variation_teinte_generale}}
	
	def affiche(self, matrice, infos, delai, start=0):
		if self.conf.serie and self.conf._serie_prete:
			"Si la sortie série est activée, on envoi ce qu'il faut, où il faut"
			self.conf.sortie_serie.flushInput()
			mesg = ""
			for x in range(0, self.conf.nb_cases):
				for y in range(0, self.conf.nb_cases):
					for color in range(0,3):
						mesg = mesg + str(matrice[x][self.conf.nb_cases-1-y][color]) + ','
			mesg = mesg[:-1] + '\n' # on nettoie la derniere virgule, et on ajoute une fin de ligne
			self.conf.sortie_serie.write(mesg.encode('ascii'))
		self.conf.socket_matrices.send_json(matrice)
		self.conf.socket_infos.send_json(infos)
		stop = time.perf_counter()
		if start != 0:
			pause = delai/1000 - (stop-start)
		else:
			pause = delai/1000
		if pause > 0:
			time.sleep(delai/1000)