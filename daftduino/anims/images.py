import time
import copy
import random
from math import *
import os

from PIL import Image # installer pillow

from .base import Anims
from couleur import Couleur

TOUT = 0
MIX = 1
RGB = 2

class Images(Anims):
	# Liste les frames d'un gif animé
	def _iter_frames(self, im):
		try:
			i= 0
			while 1:
				im.seek(i)
				imframe = im.copy()
				if i == 0: 
					palette = imframe.getpalette()
				else:
					imframe.putpalette(palette)
				yield imframe
				i += 1
		except EOFError:
			pass

class Texte(Images):
	coeff = 2
	def play(self, MatriceBase, kwargs={}):
		chemin = os.path.dirname(os.path.abspath(__file__)) + "/ressources/" + str(self.conf.nb_cases) + "/alpha/"
		if os.path.isdir(chemin): # le chemin existe, on peut y aller
			CouleurCible = Couleur()
			Textes = [
				"DO_IT|",
				"MAKE_IT|",
				"ROCK_IT|",
				"HARDER",
				"BETTER",
				"FASTER",
				"STRONGER",
				"GET_LUCKY",
				"DISCO",
				"DANCE!",
				"TECHNOLOGIC",
				"ONE_MORE_TIME",
				"WE_ARE_YOUR_FRIENDS",
				"ROBOT_ROCK"
			]
			kwargs['Texte'] = kwargs.get('Texte', Textes[random.randint(0,len(Textes)-1)])
			idx = 0
			for Caractere in kwargs['Texte'].upper():
				start = time.perf_counter()
				dic_infos = Anims.infos(self, len(kwargs['Texte']), idx, kwargs)
				idx += 1
				if Caractere == "!":# gestion des caractère spéciaux
					Caractere = "_ex"
				elif Caractere == "|":
					Caractere = "_dex"
				imgCar = chemin + Caractere + ".png"
				if os.path.isfile(imgCar):
					im = Image.open(imgCar)
				else:
					im = Image.open(chemin + "_.png")
				for x in range(0, self.conf.nb_cases):
					for y in range(0, self.conf.nb_cases):
						R, V, B = im.getpixel((x, y))
						MatriceBase[x][y] = Couleur(R,V,B) * CouleurCible
				CouleurCible.varie()
				self.affiche(MatriceBase, dic_infos, self.conf.delai*2, start)
				if self.conf.fin or self.conf.zap:
					return MatriceBase
				if self.conf.pause:
					while self.conf.pause:
						if self.conf.fin == True:
							break
						time.sleep(0.1)
		return MatriceBase

class Pictos(Images):
	def play(self, MatriceBase, kwargs={}):
		kwargs['Frames'] = kwargs.get('Frames', random.randint(5,10))
		chemin = os.path.dirname(os.path.abspath(__file__)) + "/ressources/" + str(self.conf.nb_cases) + "/pictos/"
		if os.path.isdir(chemin): # le chemin existe, on peut y aller
			CouleurCible = Couleur()
			Pictos  = [file for file in os.listdir(chemin) if file.lower().endswith('png')]
			for Frames in range(0,kwargs['Frames']):
				start = time.perf_counter()
				dic_infos = Anims.infos(self, kwargs['Frames'], Frames, kwargs)
				Picto = Pictos[random.randint(0,len(Pictos)-1)]

				imgCar = chemin + Picto
				im = Image.open(imgCar)
				for x in range(0, self.conf.nb_cases):
					for y in range(0, self.conf.nb_cases):
						R, V, B = im.getpixel((x, y))
						if Picto[-5:-4] == "N":
							MatriceBase[x][y] = Couleur(R,V,B) * CouleurCible
						else:
							MatriceBase[x][y] = Couleur(R,V,B)
				CouleurCible.varie()
				self.affiche(MatriceBase, dic_infos, self.conf.delai*2, start)
				if self.conf.fin or self.conf.zap:
					return MatriceBase
				if self.conf.pause:
					while self.conf.pause:
						if self.conf.fin == True:
							break
						time.sleep(0.1)
		return MatriceBase

class Panos(Images):
	def play(self, MatriceBase, kwargs={}):
		chemin = os.path.dirname(os.path.abspath(__file__)) + "/ressources/" + str(self.conf.nb_cases) + "/panos/"
		if os.path.isdir(chemin): # le chemin existe, on peut y aller
			CouleurCible = Couleur()
			Pictos  = [file for file in os.listdir(chemin) if file.lower().endswith('png')]
			Picto = Pictos[random.randint(0,len(Pictos)-1)]
			imgCar = chemin + Picto
			Vitesse = int(Picto[-6:-4])/10
			Orientation = Picto[-8:-7]
			if Orientation == "H":
				Dim = 0
			else:
				Dim = 1
			Sens = Picto[-7:-6]
			if Sens == "N":
				if random.randint(0,1) == 0: Sens = "G"
				else: Sens = "D"
			im = Image.open(imgCar)
			if Sens == "G": Range = range(0,im.size[Dim]-self.conf.nb_cases)
			else: Range = range(im.size[Dim]-self.conf.nb_cases, 0,-1)
			for Frames in Range:
				start = time.perf_counter()
				if Sens == "D": FramesAff = Range.start - Frames
				else:FramesAff = Frames
				dic_infos = Anims.infos(self, max(Range.start, Range.stop), FramesAff, kwargs)
				for x in range(0, self.conf.nb_cases):
					for y in range(0, self.conf.nb_cases):
						if Orientation == "H":
							R, V, B = im.getpixel((x+Frames, y))
						else:
							R, V, B = im.getpixel((x, y+Frames))
						if Picto[-9:-8] == "N":
							MatriceBase[x][y] = Couleur(R,V,B) * CouleurCible
						else:
							MatriceBase[x][y] = Couleur(R,V,B)
						MatriceBase[x][y] = Couleur(R,V,B) * CouleurCible
				CouleurCible.varie()
				self.affiche(MatriceBase, dic_infos, self.conf.delai*Vitesse, start)
				if self.conf.fin or self.conf.zap:
					return MatriceBase
				if self.conf.pause:
					while self.conf.pause:
						if self.conf.fin == True:
							break
						time.sleep(0.1)
		return MatriceBase

class Kino(Images):
	def play(self, MatriceBase, kwargs={}):
		chemin = os.path.dirname(os.path.abspath(__file__)) + "/ressources/" + str(self.conf.nb_cases) + "/kino/"
		if os.path.isdir(chemin):
			CouleurCible = Couleur()
			Kinos  = [file for file in os.listdir(chemin) if file.lower().endswith('gif')]
			Anim = Kinos[random.randint(0,len(Kinos)-1)]
			imgAnim = chemin + Anim
			Vitesse = int(Anim[-6:-4])/10
			im = Image.open(imgAnim)
			for i, frame in list(enumerate(self._iter_frames(im))):
				start = time.perf_counter()
				dic_infos = Anims.infos(self, len(list(enumerate(self._iter_frames(im)))), len(list(enumerate(self._iter_frames(im))))-i, kwargs)
				rgb_im = frame.convert('RGB')
				for x in range(0, self.conf.nb_cases):
					for y in range(0, self.conf.nb_cases):
						R, V, B = rgb_im.getpixel((x, y))
						if Anim[-7:-6] == "N":
							MatriceBase[x][y] = Couleur(R,V,B) * CouleurCible
						else:
							MatriceBase[x][y] = Couleur(R,V,B)
				CouleurCible.varie()
				self.affiche(MatriceBase, dic_infos, self.conf.delai*Vitesse, start)
				if self.conf.fin or self.conf.zap:
					return MatriceBase
				if self.conf.pause:
					while self.conf.pause:
						if self.conf.fin == True:
							break
						time.sleep(0.1)
		return MatriceBase
