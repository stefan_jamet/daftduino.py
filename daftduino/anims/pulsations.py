import time
import copy
import random
from math import *

from .base import Anims
from couleur import Couleur

TOUT = 0
MIX = 1
RGB = 2

class Pulsations(Anims):
	pass

class Pulsation(Pulsations):
	_couleur = MIX
	def play(self, MatriceBase, kwargs={}):
		kwargs['Estompage'] = kwargs.get('Estompage', 5)
		
		CouleurCible = Couleur()
		MaxDistance = sqrt((self.conf.nb_cases / 2)**2 + (self.conf.nb_cases / 2)**2)
		Tableaux = random.randint(4,9)
		for truc in range(0,Tableaux):
			CouleurCible.varie()
			CouleurTemp = Couleur(0,0,0)
			CentreX = random.randrange(0, self.conf.nb_cases)
			CentreY = random.randrange(0, self.conf.nb_cases)
			for Xeq in range(0,25):
				start = time.perf_counter()
				dic_infos = Anims.infos(self, Tableaux*25, truc*25+Xeq, kwargs)
				MatriceBase.estompe(kwargs['Estompage'])
				PulsationX = int((245/(0.5*(Xeq-17)**2+1))+(150/(0.5*(Xeq-10)**2+1)))
				for color in range(0,3):
					CouleurTemp[color] = int(CouleurCible[color] * PulsationX/255)
				for x in range(0,self.conf.nb_cases):
					for y in range(0,self.conf.nb_cases):
						Distance = sqrt((CentreX-x)**2 + (CentreY-y)**2) * 200 / MaxDistance
						MatriceBase[x][y] = MatriceBase[x][y] + CouleurTemp - Distance
				self.affiche(MatriceBase, dic_infos, self.conf.delai/4, start)
				if self.conf.fin or self.conf.zap:
					return MatriceBase
				if self.conf.pause:
					while self.conf.pause:
						if self.conf.fin == True:
							break
						time.sleep(0.1)
		return MatriceBase
class Pulsation_RGB(Pulsations):
	_couleur = RGB
	def play(self, MatriceBase, kwargs={}):
		kwargs['Estompage'] = kwargs.get('Estompage', 5)
		kwargs['Frames'] = int(kwargs.get('Frames', random.randint(100,200))/25)
		
		CouleurCible = Couleur()
		MaxDistance = sqrt((self.conf.nb_cases / 2)**2 + (self.conf.nb_cases / 2)**2)
		for Tableaux in range(0, kwargs['Frames']):
			CouleurCible.varie()
			CouleurTemp = Couleur(0,0,0)
			CentreX = [random.randrange(0, self.conf.nb_cases),random.randrange(0, self.conf.nb_cases),random.randrange(0, self.conf.nb_cases)]
			CentreY = [random.randrange(0, self.conf.nb_cases),random.randrange(0, self.conf.nb_cases),random.randrange(0, self.conf.nb_cases)]
			for Xeq in range(0,25):
				start = time.perf_counter()
				dic_infos = Anims.infos(self, kwargs['Frames']*25, Tableaux*25+Xeq, kwargs)
				MatriceBase.estompe(kwargs['Estompage'])
				PulsationX = int((245/(0.5*(Xeq-17)**2+1))+(150/(0.5*(Xeq-10)**2+1)))
				for color in range(0,3):
					CouleurTemp[color] = int(CouleurCible[color] * PulsationX/255)
				for x in range(0,self.conf.nb_cases):
					for y in range(0,self.conf.nb_cases):
						for color in range(0,3):
							Distance = sqrt((CentreX[color]-x)**2 + (CentreY[color]-y)**2) * 200 / MaxDistance
							MatriceBase[x][y][color] = MatriceBase[x][y][color] + (CouleurTemp[color] - Distance)
				self.affiche(MatriceBase, dic_infos, self.conf.delai/4, start)
				if self.conf.fin or self.conf.zap:
					return MatriceBase
				if self.conf.pause:
					while self.conf.pause:
						if self.conf.fin == True:
							break
						time.sleep(0.1)
		return MatriceBase


class Cercles(Pulsations):
	_couleur = MIX
	def play(self, MatriceBase, kwargs={}):
		kwargs['Estompage'] = kwargs.get('Estompage', 5)
		kwargs['Frames'] = int(kwargs.get('Frames', random.randint(100,200))/25)
		for Tableaux in range(0, kwargs['Frames']):
			CouleurCible = Couleur()
			CentreX = random.randrange(0, self.conf.nb_cases)
			CentreY = random.randrange(0, self.conf.nb_cases)
			for Frames in range(0, self.conf.nb_cases*5*2):
				start = time.perf_counter()
				dic_infos = Anims.infos(self, kwargs['Frames']*25, Tableaux*25+Frames, kwargs)
				MatriceBase.estompe(kwargs['Estompage'])
				for x in range(0, self.conf.nb_cases):
					for y in range(0, self.conf.nb_cases):
						for color in range(0,3):
							Rayon = Frames/5
							RayonCalc = sqrt((x-CentreX)**2+(y-CentreY)**2)
							if Rayon > RayonCalc-0.6 and Rayon < RayonCalc+0.6:
								MatriceBase[x][y][color] = Couleur.rgb_min_max(CouleurCible[color]*(1-(RayonCalc-Rayon)))
							if Rayon-self.conf.nb_cases/1.5 > RayonCalc-0.5 and Rayon-self.conf.nb_cases/1.5 < RayonCalc+0.5:
								MatriceBase[x][y][color] = Couleur.rgb_min_max(CouleurCible[color]*(1-(RayonCalc-Rayon+self.conf.nb_cases/1.5)))
				self.affiche(MatriceBase, dic_infos, self.conf.delai/4, start)
				if self.conf.fin or self.conf.zap:
					return MatriceBase
				if self.conf.pause:
					while self.conf.pause:
						if self.conf.fin == True:
							break
						time.sleep(0.1)
		return MatriceBase

class Cercles_RGB(Pulsations):
	_couleur = RGB
	def play(self, MatriceBase, kwargs={}):
		kwargs['Estompage'] = kwargs.get('Estompage', 5)
		kwargs['Frames'] = int(kwargs.get('Frames', random.randint(100,200))/25)
		
		for Tableaux in range(0, kwargs['Frames']):
			CouleurCible = Couleur()
			CentreX = [random.randrange(0, self.conf.nb_cases),random.randrange(0, self.conf.nb_cases),random.randrange(0, self.conf.nb_cases)]
			CentreY = [random.randrange(0, self.conf.nb_cases),random.randrange(0, self.conf.nb_cases),random.randrange(0, self.conf.nb_cases)]
			RayonCalc = [0,0,0]
			for Frames in range(0, self.conf.nb_cases*5*2):
				start = time.perf_counter()
				dic_infos = Anims.infos(self, kwargs['Frames']*25, Tableaux*25+Frames, kwargs)
				MatriceBase.estompe(kwargs['Estompage'])
				for x in range(0, self.conf.nb_cases):
					for y in range(0, self.conf.nb_cases):
						for color in range(0,3):
							Rayon = Frames/5
							RayonCalc[color] = sqrt((x-CentreX[color])**2+(y-CentreY[color])**2)
							if Rayon > RayonCalc[color]-0.6 and Rayon < RayonCalc[color]+0.6:
								MatriceBase[x][y][color] = Couleur.rgb_min_max(CouleurCible[color]*(1-(RayonCalc[color]-Rayon)))
							if Rayon-self.conf.nb_cases/1.5 > RayonCalc[color]-0.5 and Rayon-self.conf.nb_cases/1.5 < RayonCalc[color]+0.5:
								MatriceBase[x][y][color] = Couleur.rgb_min_max(CouleurCible[color]*(1-(RayonCalc[color]-Rayon+self.conf.nb_cases/1.5)))
				self.affiche(MatriceBase, dic_infos, self.conf.delai/4, start)
				if self.conf.fin or self.conf.zap:
					return MatriceBase
				if self.conf.pause:
					while self.conf.pause:
						if self.conf.fin == True:
							break
						time.sleep(0.1)
		return MatriceBase
