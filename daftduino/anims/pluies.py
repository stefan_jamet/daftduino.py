import time
import copy
import random
from math import *

from .base import Anims
from couleur import Couleur

TOUT = 0
MIX = 1
RGB = 2

class Pluies(Anims):
	def play_std(self, MatriceBase, kwargs={}):
		# Valeurs par defaut
		kwargs['Frames'] = kwargs.get('Frames', random.randint(100,200))
		kwargs['Estompage'] = kwargs.get('Estompage', 10)
		kwargs['Symetrie'] = kwargs.get('Symetrie', 0)

		CouleurCible = Couleur()
		for Frames in range(0,kwargs['Frames']):
			start = time.perf_counter()
			dic_infos = Anims.infos(self, kwargs['Frames'], Frames, kwargs)
			MatriceBase.estompe(kwargs['Estompage'])
			x = random.randrange(int(0-100*(1/(10+self.conf.nb_cases*self.conf.nb_cases))), self.conf.nb_cases)
			y = random.randrange(int(0-100*(1/(10+self.conf.nb_cases*self.conf.nb_cases))), self.conf.nb_cases)
			if x >= 0 and y >= 0:
				MatriceBase[x][y] = copy.copy(CouleurCible)
				if kwargs['Symetrie'] == 1 or kwargs['Symetrie'] == 4:
					MatriceBase[x][self.conf.nb_cases-1-y] = copy.copy(CouleurCible)
				if kwargs['Symetrie'] == 2 or kwargs['Symetrie'] == 4:
					MatriceBase[self.conf.nb_cases-1-x][y] = copy.copy(CouleurCible)
				if kwargs['Symetrie'] == 3 or kwargs['Symetrie'] == 4:
					MatriceBase[self.conf.nb_cases-1-x][self.conf.nb_cases-1-y] = copy.copy(CouleurCible)
			CouleurCible = CouleurCible.alea()
			self.affiche(MatriceBase, dic_infos, self.conf.delai, start)
			if self.conf.fin or self.conf.zap:
				return MatriceBase
			if self.conf.pause:
				while self.conf.pause:
					if self.conf.fin == True:
						break
					time.sleep(0.1)
		return MatriceBase
	def play_rgb(self, MatriceBase, kwargs={}):
		# Valeurs par defaut
		kwargs['Frames'] = kwargs.get('Frames', random.randint(100,200))
		kwargs['Estompage'] =args= kwargs.get('Estompage', 10)
		kwargs['Symetrie'] = kwargs.get('Symetrie', 0)

		for Frames in range(0,kwargs['Frames']):
			start = time.perf_counter()
			dic_infos = Anims.infos(self, kwargs['Frames'], Frames, kwargs)
			CouleurCible = Couleur()
			MatriceBase.estompe(kwargs['Estompage'])
			for color in range(0,3):
				x = random.randrange(0,self.conf.nb_cases)
				y = random.randrange(0,self.conf.nb_cases)
				MatriceBase[x][y][color] += copy.copy(CouleurCible[color])
				if kwargs['Symetrie'] == 1 or kwargs['Symetrie'] == 4:
					MatriceBase[x][self.conf.nb_cases-1-y][color] = copy.copy(CouleurCible[color])
				if kwargs['Symetrie'] == 2 or kwargs['Symetrie'] == 4:
					MatriceBase[self.conf.nb_cases-1-x][y][color] = copy.copy(CouleurCible[color])
				if kwargs['Symetrie'] == 3 or kwargs['Symetrie'] == 4:
					MatriceBase[self.conf.nb_cases-1-x][self.conf.nb_cases-1-y][color] = copy.copy(CouleurCible[color])
			CouleurCible = CouleurCible.alea()
			self.affiche(MatriceBase, dic_infos, self.conf.delai, start)
			if self.conf.fin or self.conf.zap:
				return MatriceBase
			if self.conf.pause:
				while self.conf.pause:
					if self.conf.fin == True:
						break
					time.sleep(0.1)
		return MatriceBase

class Pluie(Pluies):
	_couleur = MIX
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		obj = Pluies()
		MatriceBase = obj.play_std(MatriceBase, kwargs)
		return MatriceBase
class Pluie_RGB(Pluies):
	_couleur = RGB
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		obj = Pluies()
		MatriceBase = obj.play_rgb(MatriceBase, kwargs)
		return MatriceBase
class Pluie_Sym(Pluies):
	_couleur = MIX
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		kwargs['Symetrie'] = 4
		obj = Pluies()
		MatriceBase = obj.play_std(MatriceBase, kwargs)
		return MatriceBase
class Pluie_Sym2(Pluies):
	_couleur = MIX
	coeff = 15
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		kwargs['Symetrie'] = random.randint(1,3)
		obj = Pluies()
		MatriceBase = obj.play_std(MatriceBase, kwargs)
		return MatriceBase
class Pluie_Sym_RGB(Pluies):
	_couleur = RGB
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		kwargs['Symetrie'] = 4
		obj = Pluies()
		MatriceBase = obj.play_rgb(MatriceBase, kwargs)
		return MatriceBase
class Pluie_Sym2_RGB(Pluies):
	_couleur = RGB
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		kwargs['Symetrie'] = random.randint(1,3)
		obj = Pluies()
		MatriceBase = obj.play_rgb(MatriceBase, kwargs)
		return MatriceBase
class Pluie_Empil(Pluies):
	_couleur = MIX
	coeff = 5
	def play(self, MatriceBase, kwargs={}):
		kwargs['Estompage'] = kwargs.get('Estompage', self.conf.nb_cases*self.conf.nb_cases*self.conf.nb_cases)
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		obj = Pluies()
		MatriceBase = obj.play_std(MatriceBase, kwargs)
		return MatriceBase
class Pluie_RGB_Empil(Pluies):
	_couleur = MIX
	coeff = 1
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		kwargs['Estompage'] = kwargs.get('Estompage', self.conf.nb_cases*self.conf.nb_cases*self.conf.nb_cases)
		obj = Pluies()
		MatriceBase = obj.play_rgb(MatriceBase, kwargs)
		return MatriceBase
class Pluie_Sym_Empil(Pluies):
	_couleur = MIX
	coeff = 5
	def play(self, MatriceBase, kwargs={}):
		kwargs['Estompage'] = kwargs.get('Estompage', self.conf.nb_cases*self.conf.nb_cases*self.conf.nb_cases)
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		kwargs['Symetrie'] = 4
		obj = Pluies()
		MatriceBase = obj.play_std(MatriceBase, kwargs)
		return MatriceBase
class Pluie_Sym_RGB_Empil(Pluies):
	_couleur = MIX
	coeff = 1
	def play(self, MatriceBase, kwargs={}):
		kwargs['Estompage'] = kwargs.get('Estompage', self.conf.nb_cases*self.conf.nb_cases*self.conf.nb_cases)
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		kwargs['Symetrie'] = 4
		obj = Pluies()
		MatriceBase = obj.play_rgb(MatriceBase, kwargs)
		return MatriceBase