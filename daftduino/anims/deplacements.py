import time
import copy
import random
from math import *

from .base import Anims
from couleur import Couleur
from matrice import Matrice

TOUT = 0
MIX = 1
RGB = 2

class Deplacements(Anims):
	pass

class Pong(Deplacements):
	_couleur = MIX
	def play(self, MatriceBase, kwargs={}):
		kwargs['Estompage'] = kwargs.get('Estompage', 20)
		kwargs['Frames'] = kwargs.get('Frames', random.randint(150,200))
		definition = 100
		positionX = random.randint(0, int((self.conf.nb_cases-1)*definition))
		positionY = random.randint(0, int((self.conf.nb_cases-1)*definition))
		deplacementX = random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1])
		deplacementY = random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1])
		couleurPoint= Couleur()
		rayonMax = self.conf.nb_cases*random.randint(int(definition/3),int(definition/2.5))
		for Frames in range(0, kwargs['Frames']):
			start = time.perf_counter()
			dic_infos = Anims.infos(self, kwargs['Frames'], Frames, kwargs)
			MatriceBase.estompe(kwargs['Estompage'])
			positionX += deplacementX
			positionY += deplacementY
			if positionX < 0 or positionX > (self.conf.nb_cases-1)*definition:
				deplacementX = -deplacementX
			if positionY < 0 or positionY > (self.conf.nb_cases-1)*definition:
				deplacementY = -deplacementY
			for x in range(0, self.conf.nb_cases):
				for y in range(0, self.conf.nb_cases):
					Distance = sqrt((positionX-(x*definition))**2 + (positionY-(y*definition))**2) * (200*definition) / rayonMax
					for color in range(0,3):
						MatriceBase[x][y][color] = Couleur.rgb_min_max(MatriceBase[x][y][color] + Couleur.rgb_min_max(couleurPoint[color]- Distance/definition))

			self.affiche(MatriceBase, dic_infos, self.conf.delai/2, start)
			if self.conf.fin or self.conf.zap:
				return MatriceBase
			if self.conf.pause:
				while self.conf.pause:
					if self.conf.fin == True:
						break
					time.sleep(0.1)
			couleurPoint.varie()
		return MatriceBase

class Pong_RGB(Deplacements):
	_couleur = RGB
	def play(self, MatriceBase, kwargs={}):
		kwargs['Estompage'] = kwargs.get('Estompage', 20)
		kwargs['Frames'] = kwargs.get('Frames', random.randint(150,200))
		definition = 100
		positionX = [random.randint(0, int((self.conf.nb_cases-1)*definition)),random.randint(0, int((self.conf.nb_cases-1)*definition)),random.randint(0, int((self.conf.nb_cases-1)*definition))]
		positionY = [random.randint(0, int((self.conf.nb_cases-1)*definition)),random.randint(0, int((self.conf.nb_cases-1)*definition)),random.randint(0, int((self.conf.nb_cases-1)*definition))]
		deplacementX = [random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1]),random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1]),random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1])]
		deplacementY = [random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1]),random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1]),random.randint(int(definition/30), int(definition/10))*random.choice([-1, 1])]
		couleurPoint= Couleur()
		rayonMax = [self.conf.nb_cases*random.randint(int(definition/3),int(definition/2.5)),self.conf.nb_cases*random.randint(int(definition/3),int(definition/2.5)),self.conf.nb_cases*random.randint(int(definition/3),int(definition/2.5))]
		Distance = [0,0,0]
		for Frames in range(0, kwargs['Frames']):
			start = time.perf_counter()
			dic_infos = Anims.infos(self, kwargs['Frames'], Frames, kwargs)
			MatriceBase.estompe(kwargs['Estompage'])
			for c in range(0,3):
				positionX[c] += deplacementX[c]
				positionY[c] += deplacementY[c]
				if positionX[c] < 0 or positionX[c] > (self.conf.nb_cases-1)*definition:
					deplacementX[c] = -deplacementX[c]
				if positionY[c] < 0 or positionY[c] > (self.conf.nb_cases-1)*definition:
					deplacementY[c] = -deplacementY[c]
				for x in range(0, self.conf.nb_cases):
					for y in range(0, self.conf.nb_cases):
						Distance[c] = sqrt((positionX[c]-(x*definition))**2 + (positionY[c]-(y*definition))**2) * (200*definition) / rayonMax[c]
						MatriceBase[x][y][c] = Couleur.rgb_min_max(MatriceBase[x][y][c] + Couleur.rgb_min_max(couleurPoint[c]- Distance[c]/definition))

			self.affiche(MatriceBase, dic_infos, self.conf.delai/2, start)
			if self.conf.fin or self.conf.zap:
				return MatriceBase
			if self.conf.pause:
				while self.conf.pause:
					if self.conf.fin == True:
						break
					time.sleep(0.1)
			couleurPoint.varie()
		return MatriceBase


class Dessine(Deplacements):
	coeff = 0
	_test = True
	def play(self, MatriceBase, kwargs={}):
		if self.conf._positions == []:#WARNING a voir si ça marche ça...
			self.conf.zap = True
		kwargs['Estompage'] = kwargs.get('Estompage', 10)
		CouleurCible = Couleur()
		Frames = 0
		definition = 100
		rayonMax = self.conf.nb_cases*definition/(self.conf.nb_cases*1.5)
		posXprec = 0
		posYprec = 0
		FramesImmo = 0
		while not self.conf.fin:
			start = time.perf_counter()
			MatriceBase.estompe(kwargs['Estompage'])
			dic_infos = Anims.infos(self, FramesImmo, Frames, kwargs)
			Frames += 1
			if self.conf._positions[0][0] == posXprec and self.conf._positions[0][1] == posYprec:
				FramesImmo += 1
				if FramesImmo == 20:
					self.conf.zap = True
			else:
				FramesImmo = 0
			for x in range(0, self.conf.nb_cases):
				for y in range(0, self.conf.nb_cases):
					for coordonnees in self.conf._positions:
						#print(coordonnees)
						positionX = coordonnees[0]*definition*(self.conf.nb_cases-1)
						positionY = coordonnees[1]*definition*(self.conf.nb_cases-1)
						Distance = sqrt((positionX-(x*definition))**2 + (positionY-(y*definition))**2) * (200*definition) / rayonMax
						for color in range(0,3):
							MatriceBase[x][y][color] = Couleur.rgb_min_max(MatriceBase[x][y][color] + Couleur.rgb_min_max(CouleurCible[color]- Distance/definition))

			posXprec = self.conf._positions[0][0]
			posYprec = self.conf._positions[0][1]
			CouleurCible.varie()
			self.affiche(MatriceBase, dic_infos, self.conf.delai, start)
			if self.conf.fin or self.conf.zap:
				return MatriceBase
			if self.conf.pause:
				while self.conf.pause:
					if self.conf.fin == True:
						break
					time.sleep(0.1)
		return MatriceBase