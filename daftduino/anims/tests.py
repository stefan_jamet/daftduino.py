import time
#import copy
import random
from math import *

import os

from PIL import Image # installer pillow

from .base import Anims
from .images import Images
from couleur import Couleur

class Test(Images):
	coeff = 0
	_test = True
	def play(self, MatriceBase, kwargs={}):
		chemin = os.path.dirname(os.path.abspath(__file__)) + "/ressources/" + str(self.conf.nb_cases) + "/TestLEDs.gif"
		if os.path.isfile(chemin):
			im = Image.open(chemin)
			for i, frame in list(enumerate(self._iter_frames(im))):
				dic_infos = Anims.infos(self, len(list(enumerate(self._iter_frames(im)))), len(list(enumerate(self._iter_frames(im))))-i, kwargs)
				rgb_im = frame.convert('RGB')
				for x in range(0, self.conf.nb_cases):
					for y in range(0, self.conf.nb_cases):
						R, V, B = rgb_im.getpixel((x, y))
						MatriceBase[x][y] = Couleur(R,V,B)
				self.affiche(MatriceBase, dic_infos, self.conf.delai*2)
				self.conf.pause = True
				if self.conf.fin or self.conf.zap:
					self.conf.pause = False
					return MatriceBase
				if self.conf.pause:
					while self.conf.pause:
						if self.conf.fin == True:
							break
						time.sleep(0.1)
		self.conf.pause = False
		return MatriceBase

class Flash(Anims):
	def play(self, MatriceBase, kwargs={}):
		# Valeurs par defaut
		kwargs['Frames'] = kwargs.get('Frames', random.randint(75,150))
		kwargs['Symetrie'] = kwargs.get('Symetrie', 0)
		kwargs['Quantite'] = kwargs.get('Quantite', random.randint(int(self.conf.nb_cases/5),int(self.conf.nb_cases/2)))

		CouleurCible = Couleur()
		for Frames in range(0,kwargs['Frames']):
			start = time.perf_counter()
			dic_infos = Anims.infos(self, kwargs['Frames'], Frames, kwargs)

			for x in range(0, self.conf.nb_cases):
				for y in range(0, self.conf.nb_cases):
					MatriceBase[x][y] = Couleur(0,0,0)
			for q in range(0, kwargs['Quantite']):
				MatriceBase[random.randrange(0, self.conf.nb_cases)][random.randrange(0, self.conf.nb_cases)] = CouleurCible

			CouleurCible = CouleurCible.alea()
			self.affiche(MatriceBase, dic_infos, self.conf.delai/4, start)
			if self.conf.fin or self.conf.zap:
				return MatriceBase
			if self.conf.pause:
				while self.conf.pause:
					if self.conf.fin == True:
						break
					time.sleep(0.1)
		return MatriceBase