import time
import copy
import random
from math import *

from .base import Anims
from couleur import Couleur

TOUT = 0
MIX = 1
RGB = 2

class Defils(Anims):
	def play_std(self, MatriceBase, kwargs={}):
		kwargs['Frames'] = kwargs.get('Frames', random.randint(100,200))
		kwargs['Estompage'] = kwargs.get('Estompage', 20)
		kwargs['Orientation'] = kwargs.get('Orientation', random.randint(0,3))
		kwargs['Sens'] = kwargs.get('Sens', 0) #0 : sens = orientation, 1: sens = +/- orientation, 2: tous les sens

		Position = []
		CouleurLigne = []
		Sens = []
		for p in range(0, self.conf.nb_cases):
			Position.append(random.randint(-2*self.conf.nb_cases,-1))
			CouleurLigne.append(Couleur())
			if kwargs['Sens'] == 0:
				Sens.append(kwargs['Orientation'])
			elif kwargs['Sens'] == 1:
				orientation = (random.randint(0,1)*2 + kwargs['Orientation']) % 4
				Sens.append(orientation)
			else:
				Sens.append(random.randint(0,3))
		for Frames in range(0,kwargs['Frames']):
			start = time.perf_counter()
			dic_infos = Anims.infos(self, kwargs['Frames'], Frames, kwargs)
			MatriceBase.estompe()
			for x in range(0, self.conf.nb_cases):
				if Position[x] >=0 and Position[x] < self.conf.nb_cases:
					if Sens[x] == 0:
						MatriceBase[Position[x]][x] = MatriceBase[Position[x]][x]  + CouleurLigne[x]
					elif Sens[x] == 1:
						MatriceBase[x][Position[x]] = MatriceBase[x][Position[x]] + CouleurLigne[x]
					elif Sens[x] == 2:
						MatriceBase[self.conf.nb_cases-1-Position[x]][x] = MatriceBase[self.conf.nb_cases-1-Position[x]][x] + CouleurLigne[x]
					else:
						MatriceBase[x][self.conf.nb_cases-1-Position[x]] = MatriceBase[x][self.conf.nb_cases-1-Position[x]] + CouleurLigne[x]
				elif Position[x] > self.conf.nb_cases:
					Position[x] = random.randint(-2*self.conf.nb_cases,-1)
					CouleurLigne[x] = Couleur()
					if kwargs['Sens'] == 0:
						Sens[x] = kwargs['Orientation']
					elif kwargs['Sens'] == 1:
						orientation = (random.randint(0,1)*2+kwargs['Orientation'])%4
						Sens[x] = orientation
					else:
						Sens[x] = random.randint(0,3)
				Position[x] += 1
			self.affiche(MatriceBase, dic_infos, self.conf.delai, start)
			if self.conf.fin or self.conf.zap:
				return MatriceBase
			if self.conf.pause:
				while self.conf.pause:
					if self.conf.fin == True:
						break
					time.sleep(0.1)
		return MatriceBase
	def play_rgb(self, MatriceBase, kwargs={}):
		kwargs['Frames'] = kwargs.get('Frames', random.randint(100,200))
		kwargs['Estompage'] = kwargs.get('Estompage', 20)
		kwargs['Orientation'] = kwargs.get('Orientation', random.randint(0,3))
		kwargs['Sens'] = kwargs.get('Sens', 0) #0 : sens = orientation, 1: sens = +/- orientation, 2: tous les sens

		Position = []
		CouleurLigne = []
		Sens = []
		for p in range(0, self.conf.nb_cases):
			Position.append([random.randint(-2*self.conf.nb_cases,-1),random.randint(-2*self.conf.nb_cases,-1),random.randint(-2*self.conf.nb_cases,-1)])
			CouleurLigne.append(Couleur())
			if kwargs['Sens'] == 0:
				Sens.append([kwargs['Orientation'],kwargs['Orientation'],kwargs['Orientation']])
			elif kwargs['Sens'] == 1:
				orientation = [(random.randint(0,1)*2 + kwargs['Orientation']) % 4,(random.randint(0,1)*2 + kwargs['Orientation']) % 4,(random.randint(0,1)*2 + kwargs['Orientation']) % 4]
				Sens.append(orientation)
			else:
				Sens.append([random.randint(0,3),random.randint(0,3),random.randint(0,3)])
		for Frames in range(0,kwargs['Frames']):
			start = time.perf_counter()
			dic_infos = Anims.infos(self, kwargs['Frames'], Frames, kwargs)
			MatriceBase.estompe()
			for x in range(0, self.conf.nb_cases):
				for color in range(0,3):
					if Position[x][color] >=0 and Position[x][color] < self.conf.nb_cases:
						if Sens[x][color] == 0:
							MatriceBase[Position[x][color]][x][color] = MatriceBase[Position[x][color]][x][color]  + CouleurLigne[x][color]
						elif Sens[x][color] == 1:
							MatriceBase[x][Position[x][color]][color] = MatriceBase[x][Position[x][color]][color] + CouleurLigne[x][color]
						elif Sens[x][color] == 2:
							MatriceBase[self.conf.nb_cases-1-Position[x][color]][x][color] = MatriceBase[self.conf.nb_cases-1-Position[x][color]][x][color] + CouleurLigne[x][color]
						else:
							MatriceBase[x][self.conf.nb_cases-1-Position[x][color]][color] = MatriceBase[x][self.conf.nb_cases-1-Position[x][color]][color] + CouleurLigne[x][color]
					elif Position[x][color] > self.conf.nb_cases:
						Position[x][color] = random.randint(-2*self.conf.nb_cases,-1)
						CouleurTemp = Couleur()
						CouleurLigne[x][color] = CouleurTemp[color]
						if kwargs['Sens'] == 0:
							Sens[x][color] = kwargs['Orientation']
						elif kwargs['Sens'] == 1:
							orientation = (random.randint(0,1)*2+kwargs['Orientation'])%4
							Sens[x][color] = orientation
						else:
							Sens[x][color] = random.randint(0,3)
					Position[x][color] += 1
			self.affiche(MatriceBase, dic_infos, self.conf.delai, start)
			if self.conf.fin or self.conf.zap:
				return MatriceBase
			if self.conf.pause:
				while self.conf.pause:
					if self.conf.fin == True:
						break
					time.sleep(0.1)
		return MatriceBase

class Defil(Defils):
	_couleur = MIX
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		obj = Defils()
		MatriceBase = obj.play_std(MatriceBase, kwargs)
		return MatriceBase
class Defil_RGB(Defils):
	_couleur = RGB
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		obj = Defils()
		MatriceBase = obj.play_rgb(MatriceBase, kwargs)
		return MatriceBase
class Defil_Toutes_Orientations(Defils):
	_couleur = MIX
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		kwargs['Sens'] = 2
		obj = Defils()
		MatriceBase = obj.play_std(MatriceBase, kwargs)
		return MatriceBase
class Defil_Toutes_Orientations_RGB(Defils):
	_couleur = RGB
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		kwargs['Sens'] = 2
		obj = Defils()
		MatriceBase = obj.play_rgb(MatriceBase, kwargs)
		return MatriceBase
class Defil_One_Orientation(Defils):
	_couleur = MIX
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		kwargs['Sens'] = 1
		obj = Defils()
		MatriceBase = obj.play_std(MatriceBase, kwargs)
		return MatriceBase
class Defil_One_Orientation_RGB(Defils):
	_couleur = RGB
	def play(self, MatriceBase, kwargs={}):
		kwargs['_nom'] = kwargs.get('_nom', str(self.__class__.__name__).replace('_', ' '))
		kwargs['Sens'] = 1
		obj = Defils()
		MatriceBase = obj.play_rgb(MatriceBase, kwargs)
		return MatriceBase