import zmq
import threading
import json

import serial

import time

from matrice import Matrice
from playlist import Playlist
from menu import Menu
from couleur import Couleur

from ddorest import DDoREST

from config import config_generale
from logger import logger

class CoreDDo():
	def __init__(self, cases, delai, port_com):
		logger.info("on initialise la bete")
		logger.info("nb de cases :"+ str(cases))
		logger.info("delai :"+ str(delai))
		logger.info("port_com :"+ str(port_com))
		logger.info("port_infos :"+ str(port_com+1))
		logger.info("port_matrices :"+ str(port_com+2))
		
		self.conf = config_generale
		self.conf.nb_cases = cases
		self.conf.delai = delai
		
		self.playlist = Playlist()
		self.menu = Menu()
		self.menu = self.menu.genere_menu()
		
		context = zmq.Context()
		
		self.socket_reception = context.socket(zmq.REP)
		self.socket_reception.bind("tcp://*:%s" % str(port_com))
		
		self.conf.socket_infos = context.socket(zmq.PUB)
		self.conf.socket_infos.bind("tcp://*:%s" % str(port_com+1))
		
		self.conf.socket_matrices = context.socket(zmq.PUB)
		self.conf.socket_matrices.bind("tcp://*:%s" % str(port_com+2))

	# Thread Série pour envoyer vers l'arduino
	def thread_serie(self):
		#TODO mettre des variables pour le port et le baudrate, définissables avec les options argparse
		self.conf.sortie_serie = serial.Serial(port='/dev/ttyACM0', baudrate=115200, bytesize=8, parity='N', stopbits=1, timeout=1)
		self.conf.sortie_serie.close()
		self.conf.sortie_serie.open()
		retour = "."
		self.conf.sortie_serie.write(retour.encode('ascii'))
		time.sleep(0.1)
		self.conf._serie_prete = True
		while self.conf.serie:
			time.sleep(0.5)
		self.conf.sortie_serie.close()

	def reception(self):
		while not self.conf.fin:
			#  Wait for next request from client
			message = self.socket_reception.recv_json()
			retour = self._traitement(message)
			self.socket_reception.send_json(retour)

	def _traitement(self, message):
		if "__ordre__" in message:
			if message["__ordre__"] == "stop":
				self.conf.fin = True
				self.conf.socket_infos.send_json({'__info__':{'stop':True}})
				logger.info("Stop")
				return "stop"
			if message["__ordre__"] == "pause":
				etat = message['arguments']
				if etat != self.conf.pause:
					self.conf.pause = etat
					self.conf.socket_infos.send_json({'__info__':{'pause':self.conf.pause}})
					logger.info("pause : "+ str(self.conf.pause))
				return "pause"
			if message["__ordre__"] == "delai":
				delai = message['arguments']
				if delai.isdigit():
					if int(delai) < 10 : delai = "10"
					if int(delai) > 1500 : delai = "1500"
					self.conf.delai = int(delai)
					logger.info("delai : "+ str(self.conf.delai))
					return "delai"
				else:
					return "Erreur delai"
			if message["__ordre__"] == "teinte":
				teinte = message['arguments']
				Couleur.teinte_generale = float(teinte)
				logger.info("teinte : "+ str(Couleur.teinte_generale))
				return "teinte"
			if message["__ordre__"] == "delta_teinte":
				delta_teinte = message['arguments']
				Couleur.delta_teinte_generale = float(delta_teinte)
				logger.info("delta_teinte_generale : "+ str(Couleur.delta_teinte_generale))
				return "delta_teinte"
			if message["__ordre__"] == "variation_teinte":
				variation_teinte = message['arguments']
				Couleur.variation_teinte_generale = float(variation_teinte)
				logger.info("variation_teinte_generale : "+ str(Couleur.variation_teinte_generale))
				return "variation_teinte"
			if message["__ordre__"] == "zap":
				argument = message['arguments']
				if argument != None:
					self.playlist.ajout(argument[0], args=argument[1], position=0)
					self.conf.zap = True
					self.conf.socket_infos.send_json({'__info__':{'zap':argument}})
					logger.info("zap : "+ str(argument))
				else:
					self.conf.zap = True
					self.conf.socket_infos.send_json({'__info__':{'zap':True}})
					logger.info("zap : "+ str(True))
				return "zap"
			if message["__ordre__"] == "ajout":
				argument = message['arguments']
				if argument != None:
					self.playlist.ajout(argument[0], args=argument[1])
				self.conf.socket_infos.send_json({'__info__':{'ajout':argument}})
				return "ajout"
			if message["__ordre__"] == "serie":
				etat = message['arguments']
				if etat == True:
					self.conf.serie = True
					"là, on balance une fonction qui active la sortie serie"
					thread_serie = threading.Thread(target=self.thread_serie)
					thread_serie.start()
					self.conf.socket_infos.send_json({'__info__':{'serie':self.conf.serie}})
					logger.info("serie : "+ str(self.conf.serie))
				else:
					self.conf.serie = False
					self.conf._serie_prete = False
				return "serie"
			if message["__ordre__"] == "couleur":
				etat = message['arguments']%3
				if etat != self.conf._couleur:
					self.conf._couleur = etat
					self.conf.socket_infos.send_json({'__info__':{'couleur':self.conf._couleur}})
					logger.info("couleur : "+ str(self.conf._couleur))
				return "couleur"
			if message["__ordre__"] == "audio":
				etat = message['arguments']
				if etat != self.conf._audio:
					self.conf._audio = etat
					self.conf.socket_infos.send_json({'__info__':{'audio':self.conf._audio}})
					logger.info("audio : "+ str(self.conf._audio))
				return "audio"
			if message["__ordre__"] == "test":
				etat = message['arguments']
				if etat != self.conf._test:
					self.conf._test = etat
					self.conf.socket_infos.send_json({'__info__':{'test':self.conf._test}})
					logger.info("test : "+ str(self.conf._test))
				return "test"
			if message["__ordre__"] == "bloque":
				etat = message['arguments']
				if etat != self.conf.bloque:
					self.conf.bloque = etat
					self.conf.socket_infos.send_json({'__info__':{'bloque':self.conf.bloque}})
					logger.info("bloque : "+ str(self.conf.bloque))
				return "bloque"
			if message["__ordre__"] == "menu_suivant":
				self.menu.suivant()
				return self.menu.affiche()
			if message["__ordre__"] == "menu_precedent":
				self.menu.precedent()
				return self.menu.affiche()
			if message["__ordre__"] == "menu_select":
				zap = message['arguments']
				self.menu = self.menu.select()
				if self.menu.sous_menus == []:
					if zap:
						self.playlist.ajout(str(self.menu.classe), {}, position=0)
						self.conf.zap = True
						argument = [str(self.menu.classe), "menu"]
						self.conf.socket_infos.send_json({'__info__':{'zap':argument}})
						logger.info("zapmenu : "+ str(argument))
					else:
						self.playlist.ajout(str(self.menu.classe), {})
						argument = [str(self.menu.classe), "menu"]
						self.conf.socket_infos.send_json({'__info__':{'ajout':argument}})
						logger.info("ajoutmenu : "+ str(argument))
					self.menu = self.menu.retour()
				return self.menu.affiche()
			if message["__ordre__"] == "menu_retour":
				self.menu = self.menu.retour()
				return self.menu.affiche()
			if message["__ordre__"] == "position":
				self.conf._positions = message["arguments"]
				#print(self.conf._positions)
				return "position"
		elif "__info__" in message:
			if message["__info__"] == "delai":
				return self.conf.delai
			if message["__info__"] == "pause":
				return self.conf.pause
			if message["__info__"] == "serie":
				return self.conf.serie
			if message["__info__"] == "nb_cases":
				return self.conf.nb_cases
			if message["__info__"] == "liste_anims":
				return str(self.playlist.liste)
			if message["__info__"] == "playlist":
				return str(self.playlist)
			if message["__info__"] == "teinte":
				return Couleur.teinte_generale
			if message["__info__"] == "delta_teinte":
				return Couleur.delta_teinte_generale
			if message["__info__"] == "variation_teinte":
				return Couleur.variation_teinte_generale
			if message["__info__"] == "couleur":
				return self.conf._couleur
			if message["__info__"] == "audio":
				return self.conf._audio
			if message["__info__"] == "test":
				return self.conf._test
			if message["__info__"] == "bloque":
				return self.conf.bloque
			if message["__info__"] == "menu":
				return self.menu.affiche()
		return False
	
	
	
	def _animations(self):
		# Première matrice noire, pour commencer en douceur
		MatriceInit = Matrice(0,0,0)
		while not self.conf.fin:
			self.conf.zap = False
			suivant = self.playlist.suivant()
			anim_suivante = suivant[0]
			obj = anim_suivante()
			arguments = suivant[1]
			logger.debug(suivant)
			MatriceInit = obj.play(MatriceBase = MatriceInit, kwargs = arguments)
	
	def _background_morphing(self):
		while not self.conf.fin:
			# Lancer un morphing pour recuperer les frames et les utiliser ailleurs
			pass
	
	def _background_variations(self):
		while not self.conf.fin:
			# Lancer une variation pour recuperer les frames et les utiliser ailleurs
			pass
	
	def variation_couleur(self):
		while not self.conf.fin:
			if not self.conf.pause:
				Couleur.variation()
				time.sleep(self.conf.delai/1000)
	
	def REST(self):
		ddoRest = DDoREST()
		ddoRest.run(self, Couleur)
	
	def run(self):
		#thread_background_morphing = threading.Thread(target=self._background_morphing, daemon=True)
		#thread_background_morphing.start()
		
		#thread_background_variations = threading.Thread(target=self._background_variations, daemon=True)
		#thread_background_variations.start()
		
		thread_animations = threading.Thread(target=self._animations)
		thread_animations.start()
		
		thread_variation_couleur = threading.Thread(target=self.variation_couleur, daemon=True)
		thread_variation_couleur.start()
		
		thread_reception = threading.Thread(target=self.reception, daemon=True)
		thread_reception.start()
		
		thread_REST = threading.Thread(target=self.REST, daemon=True)
		thread_REST.start()
