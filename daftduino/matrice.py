import copy

from config import config_generale
from couleur import Couleur

class Matrice(list):
	"""
		Classe pour gérer les matrices
	"""
	def __init__(self, *args):
		""" Initialisation des matrice. Transfert ses arguments tels quel à la classe Couleur """
		self.conf = config_generale
		for x in range(0, self.conf.nb_cases):
			Ligne = []
			for y in range(0, self.conf.nb_cases):
				Ligne.append(Couleur(*args))
			self.append(Ligne)

	def estompe(self, Coeff = 6):
		if Coeff != 0:
			for x in range(0, self.conf.nb_cases):
				for y in range(0, self.conf.nb_cases):
					self[x][y] = copy.copy(self[x][y] - 255/(Coeff*self.conf.nb_cases/5)) # pour que la diode s'éteigne au pire après Coeff estompages
