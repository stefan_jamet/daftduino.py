import random
import json

from anims import *

from config import config_generale

classe_base = vars()['Anims']

TOUT = 0
MIX = 1
RGB = 2

class ListeGenerale():
	def _test_sous_classe(self, sous_classe):
		""" Vérifie si on doit afficher cette animation, en fonction des flags définis """
		if sous_classe._test and not self.conf._test:
			return False
		if sous_classe._audio and not self.conf._audio:
			return False
		if self.conf._couleur != TOUT and sous_classe._couleur != TOUT and sous_classe._couleur != self.conf._couleur:
			return False
		return True

class ListeAnims(list, ListeGenerale):
	_instance = None

	def __new__(classe, *args, **kargs): 
		if classe._instance is None:
			classe._instance = list.__new__(classe, *args, **kargs)
		return classe._instance

	def __init__(self):
		self.conf = config_generale
		self.coeff = [] # liste des animations avec les coefficients multiplicateurs appliqués
		self.genere_liste()

	def genere_liste(self):
		""" Génère la liste des animations, en fonction de leurs coefficients """
		self[:] = []
		self.coeff[:] = []
		self.coeff.extend(self._liste(classe_base, True))
		self.extend(self._liste(classe_base, False))

	def _liste(self, classe, coeff):
		liste_anims = []
		for sous_classe in classe.__subclasses__():
			if self._test_sous_classe(sous_classe):
				if sous_classe.__subclasses__() != []:
					liste_anims.extend(self._liste(sous_classe, coeff))
				else:
					if coeff == True:
						for i in range(0, sous_classe.coeff):
							liste_anims.append(sous_classe)
					else:
						liste_anims.append(sous_classe)
		return liste_anims
	
	

	@property
	def aleatoire(self):
		self.genere_liste()
		return self.coeff[random.randint(0,len(self.coeff)-1)]
	
	def __str__(self):
		Retour = []
		for elt in self._liste(classe_base, False):
			Retour.append(str(elt))
		return str(sorted(Retour))