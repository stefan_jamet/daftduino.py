import json

from config import config_generale

from _liste_anims import ListeAnims

from logger import logger

class Playlist(list):
	""" classe pour gérer la playlist. fonction clear() par défaut pour nettoyer la liste """
	_instance = None

	def __new__(classe, *args, **kargs): 
		if classe._instance is None:
			classe._instance = list.__new__(classe, *args, **kargs)
		return classe._instance

	def __init__(self):
		self.liste = ListeAnims()
		self.en_cours = tuple
		self.conf = config_generale

	def ajout(self, animation, args={}, position=-1):
		""" ajouter une animation à la liste. Par defaut en dernière position """
		print(animation)
		if any(animation == str(elt).split('.')[-1][:-2] for elt in self.liste): # On recherche l'animation dans la liste
		#if animation == str(elt).split('.')[-1][:-2]:
			for element in self.liste:
				if animation == str(element).split('.')[-1][:-2]:
					elt = element
					break
			anim_a_ajouter = self.liste[self.liste.index(elt)]
			logger.info("ajout : "+ str(anim_a_ajouter) + ", "+ str(args))
			if position == -1:
				self.append((anim_a_ajouter, args))
			else:
				self.insert(position, (anim_a_ajouter, args))
		else:
			logger.warning("Quelqu'un tente d'ajouter une animation pas valable" + animation)

	#def _retire_premier(self):
		#""" retirer le premier element de la liste. A appeler quand l'animation en question est lancée """
		#if len(self) > 0:
			#self.pop(0)

	def suivant(self):
		""" donne l'animation suivante et ses arguments, en fonction de ce qu'il y a dans la playlist """
		if self.conf.bloque:
			return self.en_cours
		elif len(self) == 0:
			self.en_cours = (self.liste.aleatoire,{})
			return self.en_cours
		else:
			self.en_cours = self[0]
			self.pop(0)
			return self.en_cours
	
	def __str__(self):
		Retour = []
		for elt in self:
			Retour.append(str(elt))
		return str(Retour)