#!/usr/bin/env python

import argparse

from coreddo import CoreDDo

VERSION = "3.1"

CASES = 5
DELAI = 200
PORT_COM = 6660

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Lance le serveur DaftDuino', add_help=False)
	parser.add_argument('-h', '--help', '--aide', action='help', help="Affiche cette aide, et s'en va")
	parser.add_argument('-c', '--cases', action='store', type=int, help="Nombre de cases par frame", default=CASES, dest='CASES', metavar='X')
	parser.add_argument('-d', '--delai', action='store', type=int, help="Delai d'affichage des frames (en ms)",  default=DELAI, dest='DELAI', metavar='XXX')
	parser.add_argument('-p', '--port', action='store', type=int, help="Premier port à utiliser pour la communication avec les modules. Les deux ports suivants seront aussi utilisés", default=PORT_COM, dest='PORT_COM', metavar='XXX')
	parser.add_argument('-v', '--verbose', action='count', help="Mode bavard")
	parser.add_argument('--version', action='version', help="Affiche la version du programme", version='%(prog)s '+ VERSION)
	
	args = parser.parse_args()

	core = CoreDDo(cases=args.CASES, delai=args.DELAI, port_com=args.PORT_COM)
	core.run()
