from http.server import HTTPServer, BaseHTTPRequestHandler
import json
from urllib.parse import urlparse


class MyHTTPServer(HTTPServer):

	def __init__(self, server_address, handler, core, couleur):
		HTTPServer.__init__(self, server_address, handler)
		self.core = core
		self.couleur = couleur

class DDoRESTHandler(BaseHTTPRequestHandler):
	def __init__(self, request, client_address, server):
		self.route = {
			'/couleur/teinte' : DDoRESTHandler.couleur_teinte,
			'/couleur/delta' : DDoRESTHandler.couleur_delta,
			'/couleur/varia' : DDoRESTHandler.couleur_varia,
			
			'/playlist' : DDoRESTHandler.playlist_playlist,
			'/playlist/add' : DDoRESTHandler.playlist_add,
			'/playlist/zap' : DDoRESTHandler.playlist_zap,
			'/playlist/anims_dispo' : DDoRESTHandler.playlist_animsdispo,
			
			'/anim/delai' : DDoRESTHandler.anim_delai,
			'/anim/pause' : DDoRESTHandler.anim_pause,
			'/anim/bloque' : None,
			'/anim/serial' : None,
			'/anim/filtre/couleur' : None,
			'/anim/filtre/test' : None,
			'/anim/filtre/audio' : None,
			'/anim/nb_cases' : None,
			'/anim/matrice' : None,
			
			'/anim/positions' : None,
			
			'/server/status' : None,
			'/server/stop' : None,
		}
		super().__init__(request, client_address, server)
	
	def do_GET(self):
		self.send_response(200, message="OK")
		self.send_header("Access-Control-Allow-Origin", "http://192.168.1.47")
		self.send_header("Content-type", "application/json; charset=utf-8")
		self.end_headers()
		url = urlparse(self.path)
		#print(url)
		if url.path in self.route.keys():
			self.route[url.path](self)
	
	def playlist_animsdispo(self):
		liste_anim = []
		l_anims = self.server.core.playlist.liste
		for anim in range(0, len(l_anims)):
			elt = str(l_anims[anim]).split('.')[-1][:-2]
			liste_anim.append(elt)
		self.wfile.write(json.dumps(liste_anim).encode('utf-8'))

	def playlist_playlist(self):
		liste_anim = []
		l_anims = self.server.core.playlist
		for anim in range(0, len(l_anims)):
			elt = str(l_anims[anim]).split('.')[-1][:-7]
			liste_anim.append(elt)
		print(liste_anim)
		self.wfile.write(json.dumps(liste_anim).encode('utf-8'))

	def playlist_zap(self):
		url = urlparse(self.path)
		if url.query:
			self.server.core.playlist.ajout(url.query, args={}, position=0)
		self.server.core.conf.zap = True
		
	def playlist_add(self):
		url = urlparse(self.path)
		self.server.core.playlist.ajout(url.query, args={})
	
	def anim_delai(self):
		url = urlparse(self.path)
		if url.query:
			self.server.core.conf.delai = int(url.query)
		self.wfile.write(json.dumps(self.server.core.conf.delai).encode('utf-8'))
		
	def couleur_teinte(self):
		url = urlparse(self.path)
		if url.query:
			self.server.couleur.teinte_generale = float(url.query)
		self.wfile.write(json.dumps(self.server.couleur.teinte_generale).encode('utf-8'))
	
	def couleur_delta(self):
		url = urlparse(self.path)
		if url.query:
			self.server.couleur.delta_teinte_generale = float(url.query)
		self.wfile.write(json.dumps(self.server.couleur.delta_teinte_generale).encode('utf-8'))
	
	def couleur_varia(self):
		url = urlparse(self.path)
		if url.query:
			self.server.couleur.variation_teinte_generale = float(url.query)
		self.wfile.write(json.dumps(self.server.couleur.variation_teinte_generale).encode('utf-8'))
	
	def anim_pause(self):
		url = urlparse(self.path)
		if url.query:
			if url.query == "on":
				self.server.core.conf.pause = True
			else:
				self.server.core.conf.pause = False
		else:
			self.wfile.write(json.dumps(self.server.core.conf.pause).encode('utf-8'))


class DDoREST():
		
	def run(self, core, couleur, server_class=MyHTTPServer, handler_class=DDoRESTHandler):
		server_address = ('', 8000)
		httpd = server_class(server_address, handler_class, core, couleur)
		httpd.serve_forever()



#obj = DDoREST()
#obj.run()