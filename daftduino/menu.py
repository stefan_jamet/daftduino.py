import json

from anims import *

from config import config_generale

classe_base = vars()['Anims']

from _liste_anims import ListeGenerale

from logger import logger

class Menu(ListeGenerale):
	""" classe pour gérer la playlist. fonction clear() par défaut pour nettoyer la liste """

	def __init__(self):
		self.conf = config_generale
		self.idxSelected = 0
		self.nom = None
		self.parent = None
		self.sous_menus = []
		self.classe = None

	def genere_menu(self):
		return self._menu(classe_base)
	
	def _menu(self, classe, parent=None):
		menu = Menu()
		menu.classe = classe
		menu.nom = str(classe).split('.')[-1][:-2]
		if parent == None:
			menu.parent = menu
		else:
			menu.parent = parent
		for sous_classe in classe.__subclasses__():
			if self._test_sous_classe(sous_classe):
				#if sous_classe.__subclasses__() != []:
				menu.sous_menus.append(self._menu(sous_classe, menu))
		return menu
	
	def affiche(self):
		retour = self.nom
		for elt in self.sous_menus:
			if self.sous_menus[self.idxSelected] == elt:
				retour += "\n->\t" + elt.nom
			else:
				retour += "\n\t" + elt.nom
		return retour

	def suivant(self):
		self.idxSelected = (self.idxSelected+1) % len(self.sous_menus)

	def precedent(self):
		self.idxSelected = (self.idxSelected-1) % len(self.sous_menus)

	def select(self):
		return self.sous_menus[self.idxSelected]
	
	def retour(self):
		return self.parent

			