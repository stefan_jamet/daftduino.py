var SERVEUR;

function ValidateIPaddress(ipaddress) {  
	if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)) {  
		return (true)
	}  
	return (false)  
}  

function setCookie(sName, sValue) {
	var today = new Date(), expires = new Date();
	expires.setTime(today.getTime() + (365*24*60*60*1000));
	document.cookie = sName + "=" + encodeURIComponent(sValue) + ";expires=" + expires.toGMTString();
}

function getCookie(sName) {
	var oRegex = new RegExp("(?:; )?" + sName + "=([^;]*);?");

	if (oRegex.test(document.cookie)) {
		return decodeURIComponent(RegExp["$1"]);
	} else {
		return null;
	}
}

function resetCookie(sName) {
	var today = new Date(), expires = new Date();
	expires.setTime(today.getTime() - 1000);
	document.cookie = sName + "=" + encodeURIComponent('nada') + ";expires=" + expires.toGMTString();
}

$("#cookie_absent").hide();
$("#cookie_present").hide();


if (navigator.cookieEnabled) {
	$("#error").hide();
	if (ValidateIPaddress(getCookie('ip_adress'))) {
		var ddo_server_ip = getCookie('ip_adress');
		SERVEUR = 'http://' + ddo_server_ip + ':8000'
		$("#cookie_present").show();
		aff_kinetic();
	} else {
		$("#cookie_absent").show();
	}
} else {
	$("#error").html("Vous devez accepter les cookies ! C'est bon en plus");
}

$("#form_ip_address").submit(function(e){
	var ddo_server_ip = $("#ip_adress_input").val();
	if (ValidateIPaddress(ddo_server_ip)) {
		setCookie('ip_adress', ddo_server_ip);
		$("#cookie_absent").hide();
		$("#cookie_present").show();
		e.preventDefault();
		SERVEUR = 'http://' + ddo_server_ip + ':8000';
		aff_kinetic();
	}
});
$("#form_ip_reset").submit(function(e){
	resetCookie('ip_adress');
	$.ajax({
		url: "",
		context: document.body,
		success: function(s,x){
			$(this).html(s);
		}
	});
});

function aff_prefs() {
	function HSVtoRGB(h, s, v) {
		var r, g, b, i, f, p, q, t;
		if (h && s === undefined && v === undefined) {
			s = h.s, v = h.v, h = h.h;
		}
		i = Math.floor(h * 6);
		f = h * 6 - i;
		p = v * (1 - s);
		q = v * (1 - f * s);
		t = v * (1 - (1 - f) * s);
		switch (i % 6) {
			case 0: r = v, g = t, b = p; break;
			case 1: r = q, g = v, b = p; break;
			case 2: r = p, g = v, b = t; break;
			case 3: r = p, g = q, b = v; break;
			case 4: r = t, g = p, b = v; break;
			case 5: r = v, g = p, b = q; break;
		}
		return {
			r: Math.floor(r * 255),
			g: Math.floor(g * 255),
			b: Math.floor(b * 255)
		};
	};
	
	$("#pref_button").addClass("btn-primary");
	$("#home_button").removeClass("btn-primary");
	$("#kineticjs").hide();
	$("#liste_anims").hide();
	$("#ddo_options").show();
	
	function init_prefs(){
		$.getJSON( SERVEUR + "/anim/delai", function( data ) {
			$("#slider_delaiSliderVal").text(data);
			slider_delai.setValue(data, true);
		});
		$.getJSON( SERVEUR + "/couleur/teinte", function( data ) {
			slider_teinte.setValue(data*360, true);
		});
		$.getJSON( SERVEUR + "/couleur/delta", function( data ) {
			slider_delta.setValue(data*100, true);
		});
		$.getJSON( SERVEUR + "/couleur/varia", function( data ) {
			slider_varia.setValue(data, true);
		});
		$.getJSON( SERVEUR + "/anim/pause", function( data ) {
			if (data == 'true') {
				$("#pause_button").addClass("active");
			} else {
				$("#pause_button").removeClass("active");
			}
		});
	};
	init_prefs();

	var DelaiChange = function() {
		$.getJSON( SERVEUR + "/anim/delai?"+slider_delai.getValue(), function( data ) {
			$("#slider_delaiSliderVal").text(data);
		});
	};

	var TeinteChange = function() {
		var teinte = slider_teinte.getValue()/360;
		color = HSVtoRGB(teinte,1,1)
		$('#teinte_bg').css('background', 'rgb(' + color['r'] + ',' + color['g'] + ',' + color['b'] + ')')
		$.getJSON( SERVEUR + "/couleur/teinte?"+teinte, function( data ) {

		});
	};

	var DeltaChange = function() {
		$.getJSON( SERVEUR + "/couleur/delta?"+slider_delta.getValue()/100, function( data ) {

		});
	};

	var VariaChange = function() {
		$.getJSON( SERVEUR + "/couleur/varia?"+slider_varia.getValue(), function( data ) {
		});
	};
	
	var slider_delai = $('#slider_delai').slider().on('slide', DelaiChange).data('slider');
	var slider_teinte = $('#slider_teinte').slider().on('slide', TeinteChange).data('slider');
	var slider_delta = $('#slider_delta').slider().on('slide', DeltaChange).data('slider');
	var slider_varia = $('#slider_varia').slider().on('slide', VariaChange).data('slider');
	
	$("#pause_button").on('click', function(e){
		if ($("#pause_button").hasClass('active')) {
			$.getJSON( SERVEUR + "/anim/pause?off", function( data ) {
			});
			$("#pause_button").removeClass('active');
		} else {
			$.getJSON( SERVEUR + "/anim/pause?on", function( data ) {
			});
			$("#pause_button").addClass('active');
		}
	});
	
	$("#deconection").on('click', function(e){
		resetCookie('ip_adress');
	});
};

function aff_kinetic() {
	$("#home_button").removeClass("btn-primary");
	$("#pref_button").removeClass("btn-primary");
	$("#ddo_options").hide();
	$("#liste_anims").hide();
	$("#kineticjs").show();

	var guid = (function () {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
				.toString(16)
				.substring(1);
		}

		return function () {
			return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
				s4() + '-' + s4() + s4() + s4();
		};
	})();

	var getNewCircle = function (stage, color) {
		var circle = new Kinetic.Circle({
			x: stage.getWidth() / 2,
			y: stage.getHeight() / 2,
			radius: stage.getWidth() / 20,
			fill: color,
			stroke: 'white',
			strokeWidth: 1
		});
		return circle;
	};


	var createTimeout = function(circle, circles, layer){
		return window.setTimeout(function(){
			circle.o.remove();
			layer.draw();
			var index = circles.indexOf(circle);
			if (index > -1) {
				circles.splice(index, 1);
			}
		}, 5000);
	};


	var uuid = guid();
	var color = 'yellow';

	var stage = new Kinetic.Stage({
		container: "kineticjs",
		width: 10,
		height: 10
	});

	var layer = new Kinetic.Layer();

	var rect = new Kinetic.Rect({
		height: 10,
		width: 10,
		fill: 'black'
	});

	layer.on('mousemove', function (e) {
		handleMove({
			x: e.evt.layerX,
			y: e.evt.layerY,
			id: 0,
			uuid: uuid,
			color: color
		});
	});

	layer.on('touchmove', function (e) {
		for (var i = 0; i < e.evt.targetTouches.length; i++) {
			var touch = e.evt.targetTouches[i];
			handleMove({
				x: touch.clientX,
				y: touch.clientY,
				id: i,
				uuid: uuid,
				color: color
			})
		}
	});

	stage.add(layer);
	layer.add(rect);

	var circles = [];

	$(window).resize(function () {
		resize();
	});

	var handleMove = function (pos) {
//         pos.x = pos.x / stage.width();
//         pos.y = pos.y / stage.height();

		var circle = null;

		for (var i = 0; i < circles.length; i++) {
			if (pos.uuid == circles[i].uuid && pos.id == circles[i].id) {
				circle = circles[i];
			}
		}

		if (circle == null) {
			circle = {uuid: pos.uuid,
				id: pos.id,
				o: getNewCircle(stage, color),
				timeout: createTimeout(circle, circles, layer)
			};
			if(uuid != pos.uuid){
				circle.o.setFill(pos.color);
			}
			circles.push(circle);
			layer.add(circle.o);
		}else{
			window.clearTimeout(circle.timeout);
			circle.timeout = createTimeout(circle, circles, layer);
		}

//         pos.x = pos.x * stage.width();
//         pos.y = pos.y * stage.height();

		circle.o.position(pos);

		layer.draw();
	};

	var resize = function () {
		var min = Math.min($(window).height(), $(window).width()) - 10;
		stage.setHeight(min);
		stage.setWidth(min);
		rect.setHeight(min);
		rect.setWidth(min);

		layer.draw();
		$('#kinetic').css('width', min);
	};

	resize();
};

function select_anim(anim){
	$.getJSON( SERVEUR + "/playlist/zap?"+anim, function( data ) {
	});
	return false;
};

function add_anim(event, anim){
	event.stopPropagation();
	$.getJSON( SERVEUR + "/playlist/add?"+anim, function( data ) {
	});
	return false;
};

function aff_anims() {
	$("#home_button").addClass("btn-primary");
	$("#pref_button").removeClass("btn-primary");
	$("#kineticjs").hide();
	$("#ddo_options").hide();
	$("#liste_anims").show();
	
	$.getJSON( SERVEUR + "/playlist/anims_dispo", function( data ) {
		var html = '<div class="list-group">\n'
		data.forEach(function(item) {
			html = html + '<a class="list-group-item" onclick="javascript:select_anim(\'' + item + '\'); return false;">' + item + '<button class=" btn btn-primary pull-right btn-xs" onclick="javascript:add_anim(event, \'' + item + '\'); return false;">+</button></a>'
		});
		html = html + '</div>\n'
		$("#anims").empty().append(html);
	});
	
	$.getJSON( SERVEUR + "/playlist", function( data ) {
		var html = '<div class="list-group">\n'
		data.forEach(function(item) {
			html = html + '<div class="list-group-item">' + item + '</div>'
		});
		html = html + '</div>\n'
		$("#playlist").empty().append(html);
	});
};

$("#home_button").on('click', function(e){
	if ($("#home_button").hasClass("btn-primary")) {
		aff_kinetic();
	} else {
		aff_anims();
	}
});

$("#pref_button").on('click', function(e){
	if ($("#pref_button").hasClass("btn-primary")) {
		aff_kinetic();
	} else {
		aff_prefs();
	}
});

$("#zap_button").on('click', function(e){
	$.getJSON( SERVEUR + "/playlist/zap", function( data ) {
	});
});