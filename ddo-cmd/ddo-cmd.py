#!/usr/bin/env python

import zmq
import threading
import json
import cmd
import sys

import argparse

from logger import logger

PORT_COM = 6660

VERSION = "3.1"

def Reception():
	while True:
		message = socket_infos.recv_json()
		if "__info__" in message:
			if "stop" in message["__info__"]:# and message["__info__"]["stop"] == True:
				logger.info("Signal de fin reçu du serveur")
				print("! Serveur déconecté !")
				# sys.exit() ne fonctionne que dans le thread principal...
				sys.exit()
			print(message)
		elif "__infos_matrice__" in message:
			pass
		else:
			print(message)

def envoi_ordre(ordre, args):
	message_e = {'__ordre__':ordre, 'arguments': args}
	#print('message a envoyer : "'+ message + '"')
	socket_emission.send_json(message_e)
	#  Get the reply.
	message_r = socket_emission.recv_json()
	if message_r != ordre:
		print(message_r)
	return

def envoi_info(info, args):
	message = {'__info__':info, 'arguments': args}
	#print('message a envoyer : "'+ message + '"')
	socket_emission.send_json(message)
	#  Get the reply.
	message = socket_emission.recv_json()
	return message

def ajout(arg):
	ajout_zap(arg, False)
def zap(arg):
	ajout_zap(arg, True)
def ajout_zap(arg, zap):
	if zap == True:
		desc = "Change d'animation. Toutes les options ne sont pas disponibles pour toutes les animations"
		prog_name = "zap"
		n_args = '?'
		ordre = 'zap'
	else: 
		desc = "Ajoute une animation à la playlist. Toutes les options ne sont pas disponibles pour toutes les animations"
		prog_name = "ajout"
		n_args = None
		ordre = 'ajout'
	arguments_parse = False
	parser_ajout = argparse.ArgumentParser(description=desc, add_help=False, prog=prog_name)
	options = parser_ajout.add_argument_group("Options")
	options.add_argument('-h', '--help', '--aide', action='help', help="Affiche cette aide")
	options.add_argument('anim', help="Animation a ajouter ( nom ou N°)", nargs=n_args)
	options.add_argument('-f', '--frames', action='store', type=int, help="Nombre de frames pour l'anim", metavar="FFF")
	options.add_argument('-o', '--orientation', action='store', type=int, choices=range(0, 4), help="Orientation de l'animation", metavar="O", default = -1)
	options.add_argument('-s', '--sens', action='store', type=int, choices=range(0, 3), help="Sens de l'animation", metavar="S", default = -1)
	options.add_argument('-q', '--quantite', action='store', type=int, choices=range(1, 5), help="Nombre d'élements", metavar="Q")
	options.add_argument('--sym', action='store', type=int, choices=range(0, 6), help="ajouter une symetrie", metavar="S", default = -1)
	options.add_argument('--estompe', action='store', type=int, help="Vitesse d'estompage", metavar="EE")
	options.add_argument('--texte', action='store', type=str, help="Texte à afficher")
	try:
		args = parser_ajout.parse_args(arg.split())
		arguments_parse = True
	except SystemExit as e:
		pass
	
	if arguments_parse:
		if args.anim:
			arguments = {}
			if args.frames: arguments["Frames"] = args.frames
			if args.orientation != -1: arguments["Orientation"] = args.orientation
			if args.sens != -1: arguments["Sens"] = args.sens
			if args.quantite: arguments["Quantite"] = args.quantite
			if args.sym != -1: arguments["Symetrie"] = args.sym
			if args.estompe: arguments["Estompage"] = args.estompe
			if args.texte: arguments["Texte"] = '_'.join(args.texte)

			liste = json.loads(envoi_info('liste_anims', None))
			if args.anim.isnumeric():
				args.anim = int(args.anim)
				if args.anim < len(liste):
					envoi_ordre(ordre,(liste[args.anim], arguments))
				else:
					print("Argument hors limite")
			elif any('.'+args.anim+'\'>' in elt for elt in liste): # On recherche l'argument avec un point devant
					#et une apostrophe derrière, comme dans la liste originale, ça évite de confondre avec les différentes variantes
				idx = [i for i, item in enumerate(liste) if item.endswith('.'+args.anim+'\'>')]
				envoi_ordre(ordre,(liste[idx[0]], arguments))
			else:
				print("Erreur avec l'argument.")
		else:
			envoi_ordre('zap',None)

def generique(arg, nom, typ, defaut, aide):
	arguments_parse = False
	parser_ajout = argparse.ArgumentParser(add_help=False, prog=nom)
	options = parser_ajout.add_argument_group("Options")
	options.add_argument('-h', '--help', '--aide', action='help', help="Affiche cette aide")
	options.add_argument('generique', metavar=nom, type=typ, help=aide, nargs='?', default=defaut)
	try:
		args = parser_ajout.parse_args(arg.split())
		arguments_parse = True
	except SystemExit as e:
		pass
	
	if arguments_parse:
		if args.generique != defaut:
			envoi_ordre(nom,str(args.generique))
		else:
			print(envoi_info(nom, None))

class DaftDuinoShell(cmd.Cmd):
	intro = 'Bienvenue dans le Shell DaftDuino.\ntapez help ou ? pour voir ce qui est faisable.\n'
	prompt = '> '
	
	def emptyline(self):
		print("Hey, vous devez entrer un truc !")
	def default(self, line):
		print("*** Commande inconnue : " + line)

	# ----- commandes DaftDuino -----
	def do_bye(self, arg):
		'quitte le client'
		print('A bientôt :) !')
		return True
	def do_stop_serveur(self, arg):
		'Arête le serveur et quitte le client'
		envoi_ordre('stop', None)
		print('A bientôt :) !')
		return True
	def do_pause(self, arg):
		'active/desactive la pause'
		if arg == "on":
			envoi_ordre('pause', True)
		elif arg == "off":
			envoi_ordre('pause', False)
		else:
			print(envoi_info('pause', None))
	def do_serie(self, arg):
		'active/desactive la sortie serie'
		if arg == "on":
			envoi_ordre('serie', True)
		elif arg == "off":
			envoi_ordre('serie', False)
		else:
			print(envoi_info('serie', None))
	def do_delai(self, arg):
		'définie le délai si un nombre est passé en paramètre, sinon affiche le delai actuel'
		generique(arg, 'delai', int, -1, "Affiche ou modifie la vitesse des animations")

	def do_liste_anims(self, arg):
		'affiche la liste des animations disponibles'
		liste = json.loads(envoi_info('liste_anims', None))
		print("Animations disponibles :")
		for elt in range(0,len(liste)):
			print(str(elt) +" : " + liste[elt].split('.')[-1][:-2])
	def do_playlist(self, arg):
		'affiche la liste des animations disponibles'
		liste = json.loads(envoi_info('playlist', None))
		print("Playlist :")
		for elt in range(0,len(liste)):
			print(str(elt) +" : " + liste[elt].split('.')[-1].split(',')[0][:-2] + ", " + liste[elt].split('.')[-1].split(',')[1][2:-2])

	def do_nb_cases(self, arg):
		'affiche le nombre de cases des frames'
		print(envoi_info('nb_cases', None))
	def do_teinte(self, arg):
		'définie la teinte générale si un nombre est passé en paramètre, sinon affiche la teinte actuelle'
		generique(arg, 'teinte', float, -1, "Definit la teinte")

	def do_delta_teinte(self, arg):
		'définie le delta de la teinte générale si un nombre est passé en paramètre, sinon affiche le delta actuel'
		generique(arg, 'delta_teinte', float, -1, "Definit le delta")
		#arguments_parse = False
		#parser_ajout = argparse.ArgumentParser(description="Affiche ou modifie le delta teinte", add_help=False, prog='delta_teinte')
		#options = parser_ajout.add_argument_group("Options")
		#options.add_argument('-h', '--help', '--aide', action='help', help="Affiche cette aide")
		#options.add_argument('delta', type=float, help="Definit le delta", nargs='?', default=-1)
		#try:
			#args = parser_ajout.parse_args(arg.split())
			#arguments_parse = True
		#except SystemExit as e:
			#pass
		
		#if arguments_parse:
			#if args.delta != -1:
				#if args.delta > 0.5: args.delta = 0.5
				#if args.delta < 0: args.delta = 0
				#envoi_ordre('delta_teinte',str(args.delta))
			#else:
				#print(envoi_info('delta_teinte', None))

	def do_variation_teinte(self, arg):
		'définie la variation de la teinte générale si un nombre est passé en paramètre, sinon affiche la variation actuelle'
		generique(arg, 'variation_teinte', int, -666, "Definit la variation")
		#arguments_parse = False
		#parser_ajout = argparse.ArgumentParser(description="Affiche ou modifie la variation de teinte", add_help=False, prog='variation_teinte')
		#options = parser_ajout.add_argument_group("Options")
		#options.add_argument('-h', '--help', '--aide', action='help', help="Affiche cette aide")
		#options.add_argument('variation', type=int, help="Definit la variation", nargs='?', default=-666)
		#try:
			#args = parser_ajout.parse_args(arg.split())
			#arguments_parse = True
		#except SystemExit as e:
			#pass
		
		#if arguments_parse:
			#if args.variation != -666:
				#envoi_ordre('variation_teinte',str(args.variation))
			#else:
				#print(envoi_info('variation_teinte', None))

	def do_zap(self, arg):
		'change d\'animation'
		zap(arg)


	def do_ajout(self, arg):
		'ajoute une animation à la playlist'
		ajout(arg)

	def do_audio(self, arg):
		'active/desactive les animations audio'
		if arg == "on":
			envoi_ordre('audio', True)
		elif arg == "off":
			envoi_ordre('audio', False)
		else:
			print(envoi_info('audio', None))

	def do_test(self, arg):
		'active/desactive les animations en test'
		if arg == "on":
			envoi_ordre('test', True)
		elif arg == "off":
			envoi_ordre('test', False)
		else:
			print(envoi_info('test', None))

	def do_couleur(self, arg):
		'modifie le filtre des animations en fonction de leur gestion des couleurs (tout/mix/rgb)'
		if arg == "tout":
			envoi_ordre('couleur', 0)
		elif arg == "mix":
			envoi_ordre('couleur', 1)
		elif arg == "rgb":
			envoi_ordre('couleur', 2)
		else:
			print(envoi_info('couleur', None))
		
	def do_bloque(self, arg):
		'active/desactive les animations audio'
		if arg == "on":
			envoi_ordre('bloque', True)
		elif arg == "off":
			envoi_ordre('bloque', False)
		else:
			print(envoi_info('bloque', None))
			
	def do_menu(self,arg):
		print(envoi_info('menu', None))
		
	def do_menu_suivant(self,arg):
		envoi_ordre('menu_suivant', True)
		
	def do_menu_precedent(self,arg):
		envoi_ordre('menu_precedent', True)
		
	def do_menu_select(self,arg):
		envoi_ordre('menu_select', True)
		
	def do_menu_ajout(self,arg):
		envoi_ordre('menu_select', False)
		
	def do_menu_retour(self,arg):
		envoi_ordre('menu_retour', True)

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Client en ligne de commande pour DaftDuino', add_help=False)
	options = parser.add_argument_group("Options")
	options.add_argument('-h', '--help', '--aide', action='help', help="Affiche cette aide, et s'en va")
	options.add_argument('-p', '--port', action='store', type=int, help="Port à utiliser pour la communication avec le serveur", default=PORT_COM, dest='PORT_COM', metavar='XXX')
	options.add_argument('-hote', action='store', help="Hote du serveur", default="localhost", dest='HOTE')
	options.add_argument('-v', '--verbose', action='count', help="Mode bavard")
	options.add_argument('--version', action='version', help="Affiche la version du programme", version='%(prog)s '+ VERSION)
	batch = parser.add_argument_group("Options batch","Pour ne pas utiliser le mode interactif")
	batch.add_argument('--stop_serveur', action='store_true', help="Arête le serveur")
	batch.add_argument('--pause', action='store',nargs='?', help="Met le serveur en pause.  Si rien n'est spécifié, renvoie l'étéat de pause du serveur.", metavar='on/off', const='info')
	batch.add_argument('--liste_anims', action='store_true', help="Demande la liste des animations disponibles")
	batch.add_argument('--playlist', action='store_true', help="Demande la liste des animations en playlist")
	batch.add_argument('--delai', action='store', type=int, nargs='?', help="Demande ou definit le delai", const=-1)
	batch.add_argument('--ajout', action='store', nargs=argparse.REMAINDER, help="Ajoute une animation à la playlist")
	batch.add_argument('--zap', action='store', nargs=argparse.REMAINDER, help="Zap", default=-1)
	batch.add_argument('--teinte', action='store', type=float, nargs='?', help="Demande ou definit la teinte", const=-1)
	batch.add_argument('--delta_teinte', action='store', type=float, nargs='?', help="Demande ou definit le delta teinte", const=-1)
	batch.add_argument('--variation_teinte', action='store', type=int, nargs='?', help="Demande ou definit la variation de la teinte", const=-666)
	batch.add_argument('--nb_cases', action='store_true', help="Renvoi le nombre de cases utilisé pour les frames")
	batch.add_argument('--serie', action='store',nargs='?', help="Active/désactive la sortie série.  Si rien n'est spécifié, renvoie l'étéat de la sortie série du serveur.", metavar='on/off', const='info')
	batch.add_argument('--couleur', action='store',nargs='?', help="Modifie les filtres couleur pour les animations", metavar='tout/mix/rgb', const='info')
	batch.add_argument('--audio', action='store',nargs='?', help="Active/désactive les animations audio", metavar='on/off', const='info')
	batch.add_argument('--test', action='store',nargs='?', help="Active/désactive les animations en test", metavar='on/off', const='info')
	
	args = parser.parse_args()
	NIVEAU_LOG = logger.ERROR
	if args.verbose == 1:
		NIVEAU_LOG = logger.WARNING
	elif args.verbose == 2:
		NIVEAU_LOG = logger.INFO
	elif args.verbose == 3:
		NIVEAU_LOG = logger.DEBUG
	
	logger.setLevel(NIVEAU_LOG)
		
	context = zmq.Context()
	logger.debug("Context ZeroMQ créé")
	
	socket_emission = context.socket(zmq.REQ)
	socket_emission.connect ("tcp://" + args.HOTE + ":%s" % str(args.PORT_COM))
	logger.debug("Connection au serveur")
	
	# Traitement des options batch ici, après on sort
	if args.stop_serveur:
		DaftDuinoShell().do_stop_serveur(None)
		sys.exit()
	elif args.pause:
		DaftDuinoShell().do_pause(args.pause)
		sys.exit()
	elif args.liste_anims:
		DaftDuinoShell().do_liste_anims(None)
		sys.exit()
	elif args.playlist:
		DaftDuinoShell().do_playlist(None)
		sys.exit()
	elif args.delai:
		DaftDuinoShell().do_delai(str(args.delai))
		sys.exit()
	elif args.delta_teinte:
		DaftDuinoShell().do_delta_teinte(str(args.delta_teinte))
		sys.exit()
	elif args.variation_teinte:
		DaftDuinoShell().do_variation_teinte(str(args.variation_teinte))
		sys.exit()
	elif args.teinte:
		DaftDuinoShell().do_teinte(str(args.teinte))
		sys.exit()
	elif args.ajout:
		ajout(' '.join(args.ajout))
		sys.exit()
	elif args.zap != -1:
		zap(' '.join(args.zap))
		sys.exit()
	elif args.nb_cases:
		DaftDuinoShell().do_nb_cases(None)
		sys.exit()
	elif args.serie:
		DaftDuinoShell().do_serie(args.serie)
		sys.exit()
	
	socket_infos = context.socket(zmq.SUB)
	socket_infos.connect ("tcp://" + args.HOTE + ":%s" % str(args.PORT_COM+1))
	socket_infos.setsockopt_string(zmq.SUBSCRIBE, "")
	logger.debug("Inscription aux infos du serveur")
	
	Thread_Reception = threading.Thread(target=Reception, daemon=True)
	Thread_Reception.start()
	logger.debug("Thread de réception lancé")
	
	logger.debug("Lancement de l'interface")
	DaftDuinoShell().cmdloop()

	