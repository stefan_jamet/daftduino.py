// Définir un pointeur sur les fonctions
typedef void (*FuncType) (int Matrice[NombreCases][NombreCases][3]);
// Ajouter plusieurs fois la fonction pour augmenter son coefficient
FuncType pFonctions[] = {
  PluieEmpil,
  Snake,
  PluieSym,
  Defil,
  DefilTS,
  Pulsation
};
int TotalFonctions = 6;

void AutoPilote(){
  digitalWrite(LedVerte, HIGH);
  int FonctionAlea = random(TotalFonctions);
  pFonctions[FonctionAlea](Matrice);
}

void PluieEmpil(int MatriceArg[NombreCases][NombreCases][3]){
  int Couleur[3];
  for(byte Frames = random(100,200); Frames > 0; Frames--){
    EstompeMatrice(MatriceArg,35);
    int x = random(NombreCases);
    int y = random(NombreCases);
    Colorise(Couleur);
    for(byte c = 0; c < 3; c++){
      MatriceArg[x][y][c] = Couleur[c];
    }

    if(!AfficheMatrice(MatriceArg,1)) return;
  }
}

void Snake(int MatriceArg[NombreCases][NombreCases][3]){
  int Couleur[3];
  Colorise(Couleur);
  byte X = random(NombreCases);
  byte Y = random(NombreCases);
  byte Direction = random(4);
  for(byte Frames = random(100,200); Frames > 0; Frames--){
    EstompeMatrice(MatriceArg,6);
    byte Direction2 = random(4);
    if(Direction2 != (Direction-2)%4)
      Direction = Direction2;
    switch(Direction){
      case 0:
        Y--;
        break;
      case 1:
        X++;
        break;
      case 2:
        Y++;
        break;
      default:
        X--;
    }

    for(byte c = 0; c < 3; c++){
      MatriceArg[X%NombreCases][Y%NombreCases][c] = constrain(MatriceArg[X%NombreCases][Y%NombreCases][c] + Couleur[c],0,255);
    }

    if(!AfficheMatrice(MatriceArg,1)) return;
  }
}

void PluieSym(int MatriceArg[NombreCases][NombreCases][3]){
  int Couleur[3];
  for(byte Frames = random(100,150); Frames > 0; Frames--){
    EstompeMatrice(MatriceArg,6);
    byte X = random(NombreCases);
    byte Y = random(NombreCases);
    Colorise(Couleur);
    for(byte c = 0; c < 3; c++){
      MatriceArg[X][Y][c] = Couleur[c];
      MatriceArg[X][NombreCases-1-Y][c] = Couleur[c];
      MatriceArg[NombreCases-1-X][Y][c] = Couleur[c];
      MatriceArg[NombreCases-1-X][NombreCases-1-Y][c] = Couleur[c];
    }

    if(!AfficheMatrice(MatriceArg,1)) return;
  }
}

void Defil(int MatriceArg[NombreCases][NombreCases][3]){
  byte Direction = random(4);
  int Couleur[NombreCases][3];
  int Position[NombreCases];
  for(byte Ligne = 0; Ligne < NombreCases; Ligne++){
    Colorise(Couleur[Ligne]);
    Position[Ligne] = -1*random(NombreCases*3)-1;
  }
  for(byte Frames = random(100,150); Frames > 0; Frames--){
    EstompeMatrice(MatriceArg,6);
    for(byte X = 0; X < NombreCases; X++){
      if(Position[X] >= 0 && Position[X] < NombreCases){
        for(byte c = 0; c < 3; c++){
          int Y;
          switch(Direction){
            case 0:
              Y = Position[X];
              MatriceArg[Y][X][c] = Couleur[X][c];
              break;
            case 1:
              Y = Position[X];
              MatriceArg[X][Y][c] = Couleur[X][c];
              break;
            case 2:
              Y = NombreCases-1-Position[X];
              MatriceArg[Y][X][c] = Couleur[X][c];
              break;
            default:
              Y = NombreCases-1-Position[X];
              MatriceArg[X][Y][c] = Couleur[X][c];
          }
        }
      }else if(Position[X] > NombreCases){
        Colorise(Couleur[X]);
        Position[X] = -1*random(NombreCases*3)-1;
      }
      Position[X]++;
    }

    if(!AfficheMatrice(MatriceArg,1)) return;
  }
}

void DefilTS(int MatriceArg[NombreCases][NombreCases][3]){
  byte Direction[NombreCases];
  int Couleur[NombreCases][3];
  int Position[NombreCases];
  for(byte Ligne = 0; Ligne < NombreCases; Ligne++){
    Direction[Ligne] = random(4);
    Colorise(Couleur[Ligne]);
    Position[Ligne] = -1*random(NombreCases*3)-1;
  }
  for(byte Frames = random(100,150); Frames > 0; Frames--){
    EstompeMatrice(MatriceArg,6);
    for(byte X = 0; X < NombreCases; X++){
      if(Position[X] >= 0 && Position[X] < NombreCases){
        for(byte c = 0; c < 3; c++){
          int Y;
          switch(Direction[X]){
            case 0:
              Y = Position[X];
              MatriceArg[Y][X][c] = constrain(MatriceArg[Y][X][c] + Couleur[X][c],0,255);
              break;
            case 1:
              Y = Position[X];
              MatriceArg[X][Y][c] = constrain(MatriceArg[X][Y][c] + Couleur[X][c],0,255);
              break;
            case 2:
              Y = NombreCases-1-Position[X];
              MatriceArg[Y][X][c] = constrain(MatriceArg[Y][X][c] + Couleur[X][c],0,255);
              break;
            default:
              Y = NombreCases-1-Position[X];
              MatriceArg[X][Y][c] = constrain(MatriceArg[X][Y][c] + Couleur[X][c],0,255);
          }
        }
      }else if(Position[X] > NombreCases){
        Direction[X] = random(4);
        Colorise(Couleur[X]);
        Position[X] = -1*random(NombreCases*3)-1;
      }
      Position[X]++;
    }

    if(!AfficheMatrice(MatriceArg,1)) return;
  }
}

void Pulsation(int MatriceArg[NombreCases][NombreCases][3]){
  int Couleur[3];
  int CouleurTemp[3];
  float Distance;
  float MaxDistance = sqrt(pow(NombreCases/2,2) + pow(NombreCases/2,2));
  for(byte Tableaux = random(2,5); Tableaux > 0; Tableaux--){
    Colorise(Couleur);
    byte X = random(NombreCases);
    byte Y = random(NombreCases);
    for(byte Xeq = 0; Xeq > 25; Xeq++){
      EstompeMatrice(MatriceArg,6);
      int PulsationX = (245/(0.5*pow(Xeq-17,2)+1))+(150/(0.5*pow(Xeq-10,2)+1));
      for(byte c = 0; c < 3; c++){
        CouleurTemp[c] = Couleur[c] * PulsationX/255;
      }
      for(byte x = 0; x < NombreCases; x++){
        for(byte y = 0; y < NombreCases; y++){
          Distance = sqrt(pow(X-x,2) + pow(Y-y,2)) * 200/MaxDistance;
          for(byte c = 0; c < 3; c++){
            MatriceArg[x][y][c] = constrain(MatriceArg[x][y][c]+constrain(CouleurTemp[c]-Distance,0,255),0,255);
          }
        }
      }
      if(!AfficheMatrice(MatriceArg,1)) return;
    }
  }
}
