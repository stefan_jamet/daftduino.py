void USBPilote(){
  establishContact();  // send a byte to establish contact until receiver responds 
  RecupSerie();
}

void RecupSerie(){
  while(true){
    if(Serial.available()){
      digitalWrite(LedJaune, HIGH);
      char ch = Serial.read();
      if(ch >= '0' && ch <= '9'){ // is this an ascii digit between 0 and 9?
        // yes, accumulate the value if the fieldIndex is within range
        // additional fields are not stored
        if(fieldIndex < NUMBER_OF_FIELDS) {
          values[fieldIndex] = (values[fieldIndex] * 10) + (ch - '0'); 
        }
      }else if (ch == ','){  // comma is our separator, so move on to the next field
          fieldIndex++;   // increment field index 
      }else{
        if(fieldIndex == NUMBER_OF_FIELDS-1){ // Si on a tous les chiffres pour la matrice
          // any character not a digit or comma ends the acquisition of fields
          // in this example it's the newline character sent by the Serial Monitor
          
          int x = 0;
          int y = 0;
          int c = 0;
          
          for(int i=0; i < NUMBER_OF_FIELDS; i++){
            Matrice[x][y][c] = values[i];
            c++;
            if(c == 3){
              c = 0;
              x++;
              if(x == NombreCases){
                x = 0;
                y++;
              }
            }
          }
          Serial.print("ok");
  
          if(!AfficheMatrice(Matrice,0.001)) return;
        }
        for(int i = 0; i <= fieldIndex; i++)
          values[i] = 0; // set the values to zero, ready for the next message
        fieldIndex = 0;  // ready to start over
  
  
      }
    }
    digitalWrite(LedJaune, LOW);
  }
}

void establishContact() {

  digitalWrite(LedVerte, HIGH);
  digitalWrite(LedJaune, HIGH);
  digitalWrite(LedRouge, LOW);
  char TexteDefil[] = "DaftDuino";
  int idx = 0;
  
  while (Serial.available() <= 0) {
    Serial.print(TexteDefil[idx]);
    idx = (idx+1) % sizeof(TexteDefil);
    
    delay(300);
    if(digitalRead(ZapBouton)  == HIGH) {    
      Mode = (Mode + 1)%3;
      delay(500);
      return;
    }
  }
  while (Serial.available() <= 0) {
    char test = Serial.read();
  }
  Serial.println('.');
  digitalWrite(LedVerte, LOW);
  digitalWrite(LedJaune, LOW);
  digitalWrite(LedRouge, LOW);
}
