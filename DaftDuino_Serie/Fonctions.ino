int AfficheMatrice(int Matrice[NombreCases][NombreCases][3], float MultiplicateurDelai){
  boolean Retour = true;
  int d = 0;
  float CouleurAff;
  
  Intensite = analogRead(PinPotarIntensite);
  Intensite = Intensite/1024;
  
  Tlc.clear();
  for(byte x = 0; x < NombreCases; x++){
    for(byte y = 0; y < NombreCases; y++){
      for(byte c = 0; c < 3; c++){
          CouleurAff = (Matrice[x][y][c]*Max_RVB[c])*(4095/255)*(Intensite);
          Tlc.set(d, CouleurAff);
        d++;
      }
    }
  }
  
  Tlc.update();

  if(Mode == 0){
    Delai = analogRead(PinPotarDelai)*2+10;
    delay(Delai*MultiplicateurDelai);
  }
  
  if(digitalRead(ZapBouton)  == HIGH) {    
    Mode = (Mode + 1)%3;
    Retour = false;
    ContactEtabli = false;
    delay(500);
  }
  
  return Retour;
}

void Colorise(int Couleur[3]){
  float Teinte = random(0,100);
  Teinte = Teinte/100*6;
  if(Teinte == 6)Teinte = 0;
  int var_i = int(Teinte);
  float var_2 = (1 - 1 * (Teinte - var_i));
  float var_3 = (1 - 1 * (1 - (Teinte - var_i)));
  switch(var_i){
    case 0:
      Couleur[0] = 255;
      Couleur[1] = var_3*255;
      Couleur[2] = 0;
      break;
    case 1:
      Couleur[0] = var_2*255;
      Couleur[1] = 255;
      Couleur[2] = 0;
      break;
    case 2:
      Couleur[0] = 0;
      Couleur[1] = 255;
      Couleur[2] = var_3*255;
      break;
    case 3:
      Couleur[0] = 0;
      Couleur[1] = var_2*255;
      Couleur[2] = 255;
      break;
    case 4:
      Couleur[0] = var_3*255;
      Couleur[1] = 0;
      Couleur[2] = 255;
      break;
    default:
      Couleur[0] = 255;
      Couleur[1] = 0;
      Couleur[2] = var_2*255;
      break;
  }
}


void EstompeMatrice(int Matrice[NombreCases][NombreCases][3], byte Coeff){
  for(byte x = 0; x < NombreCases; x++){
    for(byte y = 0; y < NombreCases; y++){
      for(byte c = 0; c < 3; c++){
        Matrice[x][y][c] = Matrice[x][y][c] - (255/Coeff);
        if(Matrice[x][y][c] <= 0)Matrice[x][y][c] = 0;
      }
    }
  }
}
