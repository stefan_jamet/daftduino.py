/*
    Basic Pin setup:
    ------------                                  ---u----
    ARDUINO   13|-> SCLK (pin 25)           OUT1 |1     28| OUT channel 0
              12|                           OUT2 |2     27|-> GND (VPRG)
              11|-> SIN (pin 26)            OUT3 |3     26|-> SIN (pin 11)
              10|-> BLANK (pin 23)          OUT4 |4     25|-> SCLK (pin 13)
               9|-> XLAT (pin 24)             .  |5     24|-> XLAT (pin 9)
               8|                             .  |6     23|-> BLANK (pin 10)
               7|                             .  |7     22|-> GND
               6|                             .  |8     21|-> VCC (+5V)
               5|                             .  |9     20|-> 2K Resistor -> GND
               4|                             .  |10    19|-> +5V (DCPRG)
               3|-> GSCLK (pin 18)            .  |11    18|-> GSCLK (pin 3)
               2|                             .  |12    17|-> SOUT
               1|                             .  |13    16|-> XERR
               0|                           OUT14|14    15| OUT channel 15
    ------------                                  --------

    -  Put the longer leg (anode) of the LEDs in the +5V and the shorter leg
         (cathode) in OUT(0-15).
    -  +5V from Arduino -> TLC pin 21 and 19     (VCC and DCPRG)
    -  GND from Arduino -> TLC pin 22 and 27     (GND and VPRG)
    -  digital 3        -> TLC pin 18            (GSCLK)
    -  digital 9        -> TLC pin 24            (XLAT)
    -  digital 10       -> TLC pin 23            (BLANK)
    -  digital 11       -> TLC pin 26            (SIN)
    -  digital 13       -> TLC pin 25            (SCLK)
    -  The 2K resistor between TLC pin 20 and GND will let ~20mA through each
       LED.  To be precise, it's I = 39.06 / R (in ohms).  This doesn't depend
       on the LED driving voltage.
    - (Optional): put a pull-up resistor (~10k) between +5V and BLANK so that
                  all the LEDs will turn off when the Arduino is reset.

    If you are daisy-chaining more than one TLC, connect the SOUT of the first
    TLC to the SIN of the next.  All the other pins should just be connected
    together:
        BLANK on Arduino -> BLANK of TLC1 -> BLANK of TLC2 -> ...
        XLAT on Arduino  -> XLAT of TLC1  -> XLAT of TLC2  -> ...
    The one exception is that each TLC needs it's own resistor between pin 20
    and GND.

    This library uses the PWM output ability of digital pins 3, 9, 10, and 11.
    Do not use analogWrite(...) on these pins.


    Alex Leone <acleone ~AT~ gmail.com>, 2009-02-03 */
    
    //#### Ne pas oublier de configurer la librairie Tlc5940, pour mettre 5 composants ####

#include "Tlc5940.h"

#include <string.h>

// Constantes
#define NombreCases 5 // ces 2 constantes sont des #define car elles sont appelées dans des arguments de fonction, et sinon ça veut po

const int NUMBER_OF_FIELDS = 75; // how many comma-separated fields we expect
int fieldIndex = 0;             // the current field being received
int values[NUMBER_OF_FIELDS];   // array holding values for all the fields

const byte R = 0;
const byte V = 1;
const byte B = 2;

const float Max_RVB[] = {1.0, 1.0, 0.8}; // Jouer sur ces valeurs pour corriger la balance des blancs

byte Mode = 0; // 0: Autonome; 1: Piloté par USB; 2: Audio ?
int Delai; // Temps d'affichage de chaque frame
float Intensite;
boolean ContactEtabli = false;

const byte PinPotarDelai = 0;
const byte PinPotarIntensite = 1;
const int ZapBouton = 4;
const int LedVerte = 5;
const int LedJaune = 6;
const int LedRouge = 7;

int Matrice[NombreCases][NombreCases][3];
int ValeursBase[] = {0,0,0};

void setup(){
  randomSeed(analogRead(0)+analogRead(1)+analogRead(2));// pour avoir del'aléatoire vraiment aléatoire...
  Serial.begin(115200);
  /* Call Tlc.init() to setup the tlc.
     You can optionally pass an initial PWM value (0 - 4095) for all channels.*/
  Tlc.init(0);

  delay(100);
  pinMode(LedVerte, OUTPUT);
  pinMode(LedJaune, OUTPUT);
  pinMode(LedRouge, OUTPUT);
  pinMode(ZapBouton, INPUT);
  
  digitalWrite(LedVerte, HIGH);
  digitalWrite(LedJaune, HIGH);
  digitalWrite(LedRouge, HIGH);
}

void loop(){
  digitalWrite(LedVerte, LOW);
  digitalWrite(LedJaune, LOW);
  digitalWrite(LedRouge, LOW);
  switch(Mode){
    case 1:
      USBPilote();
      break;
    case 2:
      AudioPilote();
      break;
    default:
      AutoPilote();
  }
}




/*
séquences tests :
1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75

0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,229,0,211,0,0,0,0,0,0,0,0,0,0,0,0,255,0,104,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,204,0,186,0,0,0,0,0,0,0,0,0,0,0,0,229,0,78,0,0,0,0,0,0,0,0,0,0,0,0,255,0,107,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,178,0,160,0,0,0,0,0,0,0,0,0,0,0,0,204,0,53,0,0,0,0,0,0,0,0,0,0,0,0,229,0,81,0,0,0,0,0,0,0,0,0,0,0,0,255,0,110,0,0,0,0,0,0,0,0,0,0

0,0,255,0,117,0,0,0,0,0,0,0,0,0,0,0,0,153,0,135,0,0,0,0,0,0,0,0,0,0,0,0,178,0,27,0,0,0,0,0,0,0,0,0,0,0,0,204,0,56,0,0,0,0,0,0,0,0,0,0,0,0,229,0,84,0,0,0,0,0,0,0,0,0,0

0,0,229,0,91,0,0,0,0,0,0,0,0,0,0,0,0,255,0,123,0,0,0,0,0,0,0,0,0,0,0,0,153,0,2,0,0,0,0,0,0,0,0,0,0,0,0,178,0,30,0,0,0,0,0,0,0,0,0,0,0,0,204,0,59,0,0,0,0,0,0,0,0,0,0

0,0,204,0,66,0,0,0,0,0,0,0,0,0,0,0,0,229,0,97,0,0,0,0,0,0,0,0,0,0,0,0,255,0,129,0,0,0,0,0,0,0,0,0,0,0,0,153,0,5,0,0,0,0,0,0,0,0,0,0,0,0,178,0,33,0,0,0,0,0,0,0,0,0,0
*/
