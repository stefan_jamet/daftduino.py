#!/bin/bash

./ddo-cmd/ddo-cmd.py --pause on
./ddo-cmd/ddo-cmd.py --teinte 1
./ddo-cmd/ddo-cmd.py --delta_teinte 0.01
./ddo-cmd/ddo-cmd.py --variation_teinte 1

./ddo-cmd/ddo-cmd.py --ajout Pong -f 500
./ddo-cmd/ddo-cmd.py --ajout Snake -f 250
./ddo-cmd/ddo-cmd.py --zap
./ddo-cmd/ddo-cmd.py --pause off
